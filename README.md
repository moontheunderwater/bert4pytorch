bert4pytorch项目未上线版（还在整理与调试中）

原项目地址 https://github.com/MuQiuJun-AI/bert4pytorch

 **新增加的功能** 

1.trainer,一行代码实现训练、验证、测试、预测，另实现了对抗训练（FGM,PGD），混合精度训练（fp16）等功能，后续要实现的点：r-drop，knn-bert，swa，uda等优化trick

2.预训练模型新增nezha与roberta（目前所有预训练模型仅支持中文），后续要实现的点：albert，gpt2，rorformer

3.新增examples任务：

1） bert-crf 序列标注ner 数据集 ccks2071 指标：weighted f1 94.5

2） 交互式bert文本匹配模型 数据集lcqmc 指标： acc 87.5

3） sentence-bert双塔式文本匹配模型 数据集lcqmc 指标： acc 78.8

4） 苏神的cosent 数据集lcqmc 指标： acc 79.3

5） simcse-unsup 数据集STS-B中文 指标：斯皮尔曼相关系数 71.2

6） promptbert-unsup 数据集STS-B中文 指标：斯皮尔曼相关系数 74.5

7） globalpointer 嵌套ner模型 数据集CMeEE 指标： f1 65.8

8） casrel 实体关系联合抽取模型 数据集： 百度2020信息抽取 指标 f1 76.1

10） unilm bert做生成 数据集： 指标： rouge-L

后续要实现的任务/模型： rocketqav2(dense passage retrieval), bert4gcn(aspect based sentiment analysis)

transformers复现版本：https://github.com/Changanyue/NLP_paper_implementation