#! -*- coding: utf-8 -*-
# 基本测试：bert文本分类（15类）
# 数据集：clue benchmark数据集
# 数据下载链接：https://storage.googleapis.com/cluebenchmark/tasks/tnews_public.zip

import os
import sys
import json
import torch.utils.checkpoint
sys.path.insert(0, './')
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, DataReader
from bert4pytorch.models.model_building import Bert4TorchModel

class FinetuningArguments:
    SEED = 42
    learning_rate = 4e-5
    num_train_epochs = 2
    max_len = 64
    batch_size = 512

    metric_key_for_early_stop = "accuracy"
    patience = 8
    model_name = "nezha"
    root_model_path = "D:/bert4pytorch-master/resources/NEZHA"

    dataset_name = "tnews"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
tokenizer = BertTokenizer(args.root_model_path + "/vocab.txt")

total_labels = set()
def load_data(filename):
    D = []
    with open(args.data_dir + filename, encoding='utf8') as f:
        for line in f:
            line = json.loads(line)
            try:
                sent = line['sentence']
                label = line['label_desc']
            except KeyError:
                continue
            D.append((sent, label))
            total_labels.add(label)
    return D

train_examples, dev_examples, test_examples= load_data('train.json'), load_data('dev.json'), load_data('dev.json')
label_map, num_labels = {l:i for i,l in enumerate(list(total_labels))}, len(list(total_labels))


class MyDataset(DataGenerator):
    def __getitem__(self, index):
        examp = self.examples[index]
        text, label = examp[0], examp[1]
        label_id = label_map[label]

        inputs = tokenizer.encode_plus(text, add_special_tokens=True, max_length=args.max_len)
        input_ids = inputs["input_ids"]
        attention_mask = [1] * len(input_ids)

        input_ids_tensor = sequence_padding(input_ids, args.max_len)
        att_mask_tensor = sequence_padding(attention_mask, args.max_len)
        return {'token_ids': input_ids_tensor, 'attention_mask': att_mask_tensor, 'label': label_id}


class ClsModel(Bert4TorchModel):
    def forward(self, token_ids=None, segment_ids=None, attention_mask=None, **inputs):
        _, pooled_output = self.bert(token_ids=token_ids, attention_mask=attention_mask)
        logits = self.fc(pooled_output)
        return logits


train_dataset, dev_dataset, test_dataset = MyDataset(train_examples), MyDataset(dev_examples), MyDataset(test_examples)

kwargs = {"with_pool": True, "num_labels": num_labels}
config = BertConfig.from_pretrained(args.root_model_path, **kwargs)
model = ClsModel(config=config, model_path=args.root_model_path, model_name=args.model_name, **kwargs)

trainer = Trainer(model, train_dataset, args, eval_data=dev_dataset, test_data=test_dataset,
                  label_list=list(label_map.keys()), validate_every=5, use_adv='fgm')
trainer.train()

trainer.test()
prediction_output_file = os.path.join(args.save_path, "test_predictions.csv")
trainer.predict(prediction_output_file)