#! -*- coding: utf-8 -*-
import os
import sys
import torch.nn as nn
import torch.utils.checkpoint
import torch.nn.functional as F

from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, compute_corrcoef
from bert4pytorch.models.model_building import build_transformer_model
from examples.textmatch.spearman_eval import TestDataset
sys.path.insert(0, './')

class FinetuningArguments:
    SEED = 2022
    learning_rate = 3.5e-5

    num_train_epochs = 10
    max_len = 32
    batch_size = 512
    metric_key_for_early_stop = "spearman correlation"
    patience = 10

    model_name = "bert"
    model_path = "D:/bert4pytorch-master/resources/chinese_bert_wwm_ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    dataset_name = "lcqmc"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/cosent_{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
tokenizer = BertTokenizer(args.model_path + "/vocab.txt")
# from transformers import BertTokenizerFast, BertModel, BertConfig
# tokenizer = BertTokenizerFast.from_pretrained(args.root_model_path)

total_labels = set()
def load_data(filename):
    D = []
    with open(filename, "r", encoding="utf-8") as f:
        for line in f.readlines():
            cache = line.strip().split('\t')
            if len(cache)!= 3:
                continue
            text_a, text_b, label = cache[0], cache[1], cache[-1]
            total_labels.add(label)
            D.append((text_a, label))
            D.append((text_b, label))

    return D

def load_test_data(filename):
    D = []
    with open(filename, "r", encoding="utf-8") as f:
        for line in f.readlines():
            cache = line.strip().split('\t')
            if len(cache)!=3:
                continue
            text_a, text_b, label = cache[0], cache[1], cache[-1]
            D.append((text_a, text_b, float(label)))
    return D

train_examples = load_data(args.data_dir + 'train.txt')
test_examples = load_test_data(args.data_dir + 'test.txt')
label_map, num_labels = {l:i for i,l in enumerate(list(total_labels))}, len(list(total_labels))

class MyDataset(DataGenerator):
    def __getitem__(self, index):
        examp = self.examples[index]
        text, label = examp[0], examp[1]
        label_id = label_map[label]
        text_inputs = tokenizer.encode_plus(text, add_special_tokens=True, max_length=args.max_len)
        text_input_ids = text_inputs["input_ids"]
        text_att_mask = [1] * len(text_input_ids)
        input_ids_tensor = sequence_padding(text_input_ids, args.max_len)
        att_mask_tensor = sequence_padding(text_att_mask, args.max_len)
        return {'token_ids': input_ids_tensor, 'attention_mask': att_mask_tensor, 'label': label_id}


class ClsModel(nn.Module):
    def __init__(self):
        super(ClsModel, self).__init__()
        self.config = BertConfig.from_pretrained(args.model_path)
        self.bert = build_transformer_model(config=self.config, model_path=args.model_path)
        # self.config = BertConfig.from_pretrained(args.model_path+'/config.json')
        # self.bert = BertModel.from_pretrained(args.model_path+'/pytorch_model.bin', config=self.config)

    def forward(self, token_ids=None, segment_ids=None, attention_mask=None, **inputs):
        encoded_layers, pooled_output = self.bert(input_ids=token_ids, attention_mask=attention_mask, output_hidden_states=True)
        first = encoded_layers[1]
        last = encoded_layers[-1]
        # output = self.bert(input_ids=token_ids, attention_mask=attention_mask, output_hidden_states=True)
        # # 第一层和最后一层的隐层取出  然后经过平均池化
        # first = output.hidden_states[1]  # hidden_states列表有13个hidden_state，第一个其实是embeddings，第二个元素才是第一层的hidden_state
        # last = output.hidden_states[-1]
        seq_length = first.size(1)  # 序列长度

        first_avg = torch.avg_pool1d(first.transpose(1, 2), kernel_size=seq_length).squeeze(-1)  # batch, hid_size
        last_avg = torch.avg_pool1d(last.transpose(1, 2), kernel_size=seq_length).squeeze(-1)  # batch, hid_size
        final_encoding = torch.avg_pool1d(
            torch.cat([first_avg.unsqueeze(1), last_avg.unsqueeze(1)], dim=1).transpose(1, 2), kernel_size=2).squeeze(
            -1)
        return final_encoding


train_dataset = MyDataset(train_examples)
test_dataset = TestDataset(test_examples, tokenizer, args)

class CosentTrainer(Trainer):
    def compute_loss(self, is_eval, pred, label=None, use_loss=None, input=None):
        if not is_eval:
            y_true = label
            # 1. 取出真实的标签
            y_true = y_true[::2]  # tensor([1, 0, 1]) 真实的标签

            # 2. 对输出的句子向量进行l2归一化   后面只需要对应为相乘  就可以得到cos值了
            norms = (pred ** 2).sum(axis=1, keepdims=True) ** 0.5
            # y_pred = y_pred / torch.clip(norms, 1e-8, torch.inf)
            y_pred = pred / norms

            # 3. 奇偶向量相乘
            y_pred = torch.sum(y_pred[::2] * y_pred[1::2], dim=1) * 20  # 奇/偶数位分别是是句子对的两个句子

            # 4. 取出负例-正例的差值
            y_pred = y_pred[:, None] - y_pred[None, :]  # 这里是算出所有位置 两两之间余弦的差值
            # 矩阵中的第i行j列  表示的是第i个余弦值-第j个余弦值

            # print(y_true[:, None])   [[1.],[0.],[0.],[1.],...]   (1,bs)
            # print(y_true[None, :])   [[1., 0., 0., 1.,...]]      (bs,1)
            # None是增加维度的作用，类似unsqueeze
            y_true = y_true[:, None] < y_true[None, :]  # 例如对于上面的[1.]就和[1., 0., 0., 1.,...]进行比较得到一行
            # print(y_true.size())    bs*bs  里面的值为True/False
            y_true = y_true.float()  # True/False转为1/0

            y_pred = y_pred - (1 - y_true) * 1e12
            # 标签为1的sample cosine值应大于标签为0的sample
            # 具体就是标签为0与标签为1的值为cosine差值，标签相同值为-e12，不用更新loss

            y_pred = y_pred.view(-1)
            if torch.cuda.is_available():
                y_pred = torch.cat((torch.tensor([0]).float().cuda(), y_pred),
                                   dim=0)  # 这里加0是因为e^0 = 1相当于在log中加了1，对应blog公式(6)
            else:
                y_pred = torch.cat((torch.tensor([0]).float(), y_pred), dim=0)

            return torch.logsumexp(y_pred, dim=0)
        else: return

    def evaluate_for_batch(self, model, batch):
        vec1 = model.forward(token_ids=batch['text_a_token_ids'], attention_mask=batch['text_a_att_mask'])
        vec2 = model.forward(token_ids=batch['text_b_token_ids'], attention_mask=batch['text_b_att_mask'])

        sims = F.cosine_similarity(torch.tensor(vec1), torch.tensor(vec2)).cpu().numpy()
        labels = batch['label'].cpu().numpy()

        return compute_corrcoef(sims, labels)

    def evaluate_metrics(self, is_eval, outputs, labels=None, input=None):
        if is_eval:
            return {'spearman correlation': outputs}
        else: return



model = ClsModel()
trainer = CosentTrainer(model, train_dataset, args, eval_data=test_dataset, validate_every=30, shuffle=False)

if __name__ == "__main__":
    trainer.train()
    # lcqmc  loss7.39  0.7906852422718601
    # bert4torch lqcmc 0.79386  loss 7.898897
