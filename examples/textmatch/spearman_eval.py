#! -*- coding: utf-8 -*-
import os

import torch.utils.checkpoint
import numpy as np
from tqdm import tqdm
import torch.nn.functional as F
from bert4pytorch.snippets import sequence_padding, DataGenerator, l2_normalize, compute_corrcoef



class TestDataset(DataGenerator):
    def __init__(self, examples, tokenizer, args):
        super().__init__(examples)
        self.tokenizer = tokenizer
        self.args = args

    def __getitem__(self, index):
        examp = self.examples[index]
        text_a, text_b, label = examp[0], examp[1], examp[2]

        text_a_inputs = self.tokenizer.encode_plus(text_a, add_special_tokens=True, max_length=self.args.max_len)
        text_a_input_ids = text_a_inputs["input_ids"]
        text_a_att_mask = [1] * len(text_a_input_ids)
        text_b_inputs = self.tokenizer.encode_plus(text_b, add_special_tokens=True, max_length=self.args.max_len)
        text_b_input_ids = text_b_inputs["input_ids"]
        text_b_att_mask = [1] * len(text_b_input_ids)

        text_a_input_ids_tensor = sequence_padding(text_a_input_ids, self.args.max_len).to(self.args.device)
        text_a_att_mask_tensor = sequence_padding(text_a_att_mask, self.args.max_len).to(self.args.device)
        text_b_input_ids_tensor = sequence_padding(text_b_input_ids, self.args.max_len).to(self.args.device)
        text_b_att_mask_tensor = sequence_padding(text_b_att_mask, self.args.max_len).to(self.args.device)

        return {'text_a_token_ids': text_a_input_ids_tensor, 'text_a_att_mask': text_a_att_mask_tensor,
                'text_b_token_ids': text_b_input_ids_tensor, 'text_b_att_mask': text_b_att_mask_tensor,
                'label': label}


class TestPromptDataset(DataGenerator):
    def __init__(self, examples, tokenizer, args):
        super().__init__(examples)
        self.tokenizer = tokenizer
        self.args = args

    def __getitem__(self, index):
        examp = self.examples[index]
        text_a, text_b, label = examp[0], examp[1], examp[2]
        prompt_text_a = self.args.prompt_templates[0].replace("[X]", text_a)
        prompt_text_b = self.args.prompt_templates[0].replace("[X]", text_b)

        text_a_inputs = self.tokenizer.encode_plus(prompt_text_a, add_special_tokens=True, max_length=self.args.max_len)
        text_a_input_ids = text_a_inputs["input_ids"]
        text_a_att_mask = [1] * len(text_a_input_ids)
        text_b_inputs = self.tokenizer.encode_plus(prompt_text_b, add_special_tokens=True, max_length=self.args.max_len)
        text_b_input_ids = text_b_inputs["input_ids"]
        text_b_att_mask = [1] * len(text_b_input_ids)

        text_a_input_ids_tensor = sequence_padding(text_a_input_ids, self.args.max_len).to(self.args.device)
        text_a_att_mask_tensor = sequence_padding(text_a_att_mask, self.args.max_len).to(self.args.device)
        text_b_input_ids_tensor = sequence_padding(text_b_input_ids, self.args.max_len).to(self.args.device)
        text_b_att_mask_tensor = sequence_padding(text_b_att_mask, self.args.max_len).to(self.args.device)

        return {'text_a_token_ids': text_a_input_ids_tensor, 'text_a_att_mask': text_a_att_mask_tensor,
                'text_b_token_ids': text_b_input_ids_tensor, 'text_b_att_mask': text_b_att_mask_tensor,
                'label': label}

#
# def Evaluate(model, test_dataset, args):
#     data_iterator = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=args.batch_size, shuffle=False)
#     model.load_state_dict(torch.load(args.save_path + "/model.bin", map_location=args.device))
#     model.to(args.device)
#     model.eval()
#
#     total_vec1, total_vec2, total_labels = [], [], []
#     for batch in data_iterator:
#         with torch.no_grad():
#             vec1 = model.forward(token_ids=batch['text_a_token_ids'], attention_mask=batch['text_a_att_mask'])
#             vec2 = model.forward(token_ids=batch['text_b_token_ids'], attention_mask=batch['text_b_att_mask'])
#             batch_size = vec1.size(0)
#
#             for i in range(batch_size):
#                 total_vec1.append(vec1[i].tolist())
#                 total_vec2.append(vec2[i].tolist())
#                 total_labels.append(batch['label'][i].item())
#
#     sims = F.cosine_similarity(torch.tensor(total_vec1), torch.tensor(total_vec2)).cpu().numpy()
#
#     return compute_corrcoef(sims, np.array(total_labels))

