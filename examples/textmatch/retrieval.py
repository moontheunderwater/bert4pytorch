# -*- coding: utf-8 -*-

# import torch.utils.checkpoint
from tqdm import tqdm
import torch
import torch.nn.functional as F
from sortedcontainers import SortedList
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.snippets import sequence_padding
from examples.simcse_retrieval.simcse import SimCSEModel


class FinetuningArguments:
    model_name = "bert"
    root_model_path = "D:/bert4pytorch-master/resources/chinese_bert_wwm_ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    max_len = 32
    batch_size = 128
    doc_path = 'D:/bert4pytorch-master/datasets/news_title/news_title.txt'

    simcse_model_path = 'D:/bert4pytorch-master/experiments/outputs/simcse_bert_news_title_42/model.bin'
    # saved_embeds_path = 'D:/bert4pytorch-master/experiments/saved_embeds/lawtext/text_vec.pkl'

args = FinetuningArguments()
tokenizer = BertTokenizer(args.root_model_path + "/vocab.txt")


class SimCSERetrieval(object):
    def __init__(self, fname):
        kwargs = {"with_pool": True}
        config = BertConfig.from_pretrained(args.root_model_path, **kwargs)
        model = SimCSEModel(config=config, model_path=args.root_model_path, model_name=args.model_name, pool_type='cls', **kwargs)
        model.load_state_dict(torch.load(args.simcse_model_path))
        self.model = model
        self.model.to(args.device)
        self.model.eval()

        self.fname = fname
        self.id2text = None
        self.vecs = None
        self.ids = None
        self.index = None
        self.text_vec = None

    def encode_batch(self, texts):
        token_ids, att_masks = [], []
        for txt in texts:
            text_encs = tokenizer.encode_plus(txt, max_length=args.max_len)
            input_id = sequence_padding(text_encs["input_ids"], args.max_len)
            att_mask = sequence_padding([1] * len(input_id), args.max_len)
            token_ids.append(input_id.tolist())
            att_masks.append(att_mask.tolist())

        with torch.no_grad():
            output = self.model.forward(token_ids=torch.tensor(token_ids).to(args.device), attention_mask=torch.tensor(att_masks).to(args.device))
        return output


    def embed(self):
        texts_vecs = {}
        with open(self.fname, "r", encoding="utf8") as h:
            texts = []
            for idx, line in tqdm(enumerate(h), desc='Embedding'):
                content = line.strip('\n')
                texts.append(content)

                if len(texts) >= args.batch_size:
                    vecs = self.encode_batch(texts)
                    bs = vecs.size(0)
                    for i in range(bs):
                        texts_vecs[texts[i]] = vecs[i]
                    texts = []

        self.text_vec = texts_vecs

    def sim_query(self, sentence, topK=20):
        vec = self.encode_batch([sentence])
        # vec = vec / vec.norm(dim=1, keepdim=True)

        slist = SortedList(key=lambda x: -x[1])

        for text, embed in self.text_vec.items():
            # cos = 1 - F.cosine_similarity(vec, embed).item()
            cos = torch.cosine_similarity(vec, embed).item() # .cpu().numpy()
            slist.add((text, cos))

        sim_sentences = [item[0] for item in slist][:topK]
        print([item for item in slist][:topK])
        return sim_sentences


SimModel = SimCSERetrieval(fname=args.doc_path)


SimModel.embed()
SimModel.sim_query('基金亏损路未尽 后市看法仍偏谨慎')
