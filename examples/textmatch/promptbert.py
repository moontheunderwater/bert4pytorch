#! -*- coding: utf-8 -*-
import os
import sys
import torch.nn as nn
import torch.utils.checkpoint
import torch.nn.functional as F
# from bert4pytorch.configs.configuration_bert import BertConfig
# from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, compute_corrcoef
from bert4pytorch.models.model_building import build_transformer_model
from examples.textmatch.spearman_eval import TestPromptDataset
sys.path.insert(0, './')

class FinetuningArguments:
    SEED = 2022
    learning_rate = 2.5e-5

    num_train_epochs = 10
    max_len = 120
    batch_size = 32
    metric_key_for_early_stop = "spearman correlation"
    patience = 8

    model_name = "bert"
    root_model_path = "D:/bert4pytorch-master/resources/chinese_bert_wwm_ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    replace_token = "[X]"
    mask_token = "[MASK]"
    prompt_templates = ['"{}" 的意思为[MASK]'.format(replace_token), '"{}"这句话的意思是[MASK]'.format(replace_token)]
    tao = 0.05

    dataset_name = "STS-B"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/promptbert_{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
# tokenizer = BertTokenizer(args.root_model_path + "/vocab.txt")
from transformers import BertTokenizer, BertModel, BertConfig
tokenizer = BertTokenizer.from_pretrained(args.root_model_path)
special_token_dict = {'additional_special_tokens': ['[X]']}
tokenizer.add_special_tokens(special_token_dict)
mask_id = tokenizer.convert_tokens_to_ids(args.mask_token)

def load_data(filename):
    D = []
    with open(args.data_dir + filename, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            sent = line.strip()
            words_num = len(tokenizer.tokenize(sent))
            sentence_pair = []
            for template in args.prompt_templates:
                if words_num > args.max_len - 15:
                    sent = sent[:args.max_len - 15]
                sent_num = len(tokenizer.tokenize(sent))
                prompt_sent = template.replace(args.replace_token, sent)
                template_sent = template.replace(args.replace_token, args.replace_token * sent_num)
                sentence_pair.append([prompt_sent, template_sent])
            D.append(sentence_pair)
    return D

def load_test_data(filename):
    D = []
    with open(args.data_dir + filename, "r", encoding="utf-8-sig") as f:
        for line in f.readlines():
            cache = line.split('||')
            text1, text2, label = cache[1], cache[2], cache[-1]
            D.append((text1, text2, float(label)))
    return D

# def load_data(filename):
#     def process(sent):
#         words_num = len(tokenizer.tokenize(sent))
#         sentence_pair = []
#         for template in args.prompt_templates:
#             if words_num > args.max_len - 15:
#                 sent = sent[:args.max_len - 15]
#             sent_num = len(tokenizer.tokenize(sent))
#             prompt_sent = template.replace(args.replace_token, sent)
#             template_sent = template.replace(args.replace_token, args.replace_token * sent_num)
#             sentence_pair.append([prompt_sent, template_sent])
#         return sentence_pair
#
#     D = []
#     with open(args.data_dir + filename, 'r', encoding='utf-8') as f:
#         for line in f.readlines():
#             cache = line.strip().split('\t')
#             text_a, text_b, label = cache[0], cache[1], cache[-1]
#             sentence_pair_a = process(text_a)
#             sentence_pair_b = process(text_b)
#             D.append(sentence_pair_a)
#             D.append(sentence_pair_b)
#     return D
#
#
# def load_test_data(filename):
#     D = []
#     with open(args.data_dir + filename, "r", encoding="utf-8") as f:
#         for line in f.readlines():
#             cache = line.strip().split('\t')
#             if len(cache)!=3:
#                 continue
#             text_a, text_b, label = cache[0], cache[1], cache[-1]
#             D.append((text_a, text_b, int(label)))
#     return D

train_examples = load_data('train-unsup.txt')
test_examples = load_test_data('test.txt')


class MyDataset(DataGenerator):
    def __getitem__(self, index):
        prompt_data0, prompt_data1 = self.examples[index]
        prompt_lines0, template_lines0 = prompt_data0
        prompt_lines1, template_lines1 = prompt_data1

        prompt_encodings0 = tokenizer.encode_plus(prompt_lines0, add_special_tokens=True, max_length=args.max_len)
        prompt_encodings1 = tokenizer.encode_plus(prompt_lines1, add_special_tokens=True, max_length=args.max_len)
        template_encodings0 = tokenizer.encode_plus(template_lines0, add_special_tokens=True, max_length=args.max_len)
        template_encodings1 = tokenizer.encode_plus(template_lines1, add_special_tokens=True, max_length=args.max_len)

        prompt0_input_ids_tensor = sequence_padding(prompt_encodings0['input_ids'], args.max_len)
        prompt0_att_mask_tensor = sequence_padding(prompt_encodings0['attention_mask'], args.max_len)
        prompt1_input_ids_tensor = sequence_padding(prompt_encodings1['input_ids'], args.max_len)
        prompt1_att_mask_tensor = sequence_padding(prompt_encodings1['attention_mask'], args.max_len)

        template0_input_ids_tensor = sequence_padding(template_encodings0['input_ids'], args.max_len)
        template0_att_mask_tensor = sequence_padding(template_encodings0['attention_mask'], args.max_len)
        template1_input_ids_tensor = sequence_padding(template_encodings1['input_ids'], args.max_len)
        template1_att_mask_tensor = sequence_padding(template_encodings1['attention_mask'], args.max_len)
        return {'prompt0_input': prompt0_input_ids_tensor, 'prompt0_att_mask': prompt0_att_mask_tensor, 'prompt1_input': prompt1_input_ids_tensor,
                'prompt1_att_mask': prompt1_att_mask_tensor, 'template0_att_mask': template0_att_mask_tensor, 'template0_input': template0_input_ids_tensor,
                'template1_att_mask': template1_att_mask_tensor, 'template1_input': template1_input_ids_tensor}


class PromptBERT(nn.Module):
    def __init__(self, mask_id):
        super().__init__()
        # self.config = BertConfig.from_pretrained(args.root_model_path)
        # self.bert = build_transformer_model(config=self.config, model_path=args.root_model_path)
        self.config = BertConfig.from_pretrained(args.root_model_path+'/config.json')
        self.bert = BertModel.from_pretrained(args.root_model_path+'/pytorch_model.bin', config=self.config)
        self.mask_id = mask_id

    def forward(self, prompt0_input, prompt0_att_mask, prompt1_input, prompt1_att_mask,
                template0_att_mask, template0_input, template1_att_mask, template1_input):
        key = self.get_sentence_embedding(prompt0_input, prompt0_att_mask, template0_input, template0_att_mask)
        query = self.get_sentence_embedding(prompt1_input, prompt1_att_mask, template1_input, template1_att_mask)
        return key, query

    def get_sentence_embedding(self, prompt_input_ids, prompt_attention_mask, template_input_ids,template_attention_mask):
        prompt_mask_embedding = self.calculate_mask_embedding(prompt_input_ids, prompt_attention_mask)
        template_mask_embedding = self.calculate_mask_embedding(template_input_ids, template_attention_mask)
        sentence_embedding = prompt_mask_embedding - template_mask_embedding
        return sentence_embedding

    def calculate_mask_embedding(self, input_ids, attention_mask):
        output = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        token_embeddings = output[0]
        mask_index = (input_ids == self.mask_id).long()
        mask_embedding = self.get_mask_embedding(token_embeddings, mask_index)
        return mask_embedding

    def get_mask_embedding(self, token_embeddings, mask_index):
        input_mask_expanded = mask_index.unsqueeze(-1).expand(token_embeddings.size()).float()
        mask_embedding = torch.sum(token_embeddings * input_mask_expanded, 1)
        return mask_embedding


train_dataset = MyDataset(train_examples)
test_dataset = TestPromptDataset(test_examples, tokenizer, args)

class PromptBERTTrainer(Trainer):
    def compute_loss(self, is_eval, pred, label=None, use_loss=None, input=None):
        if not is_eval:
            key, query = pred
            query = torch.div(query, torch.norm(query, dim=1).reshape(-1, 1))
            key = torch.div(key, torch.norm(key, dim=1).reshape(-1, 1))
            N, D = query.shape[0], query.shape[1]

            # calculate positive similarity
            batch_pos = torch.exp(torch.div(torch.bmm(query.view(N, 1, D), key.view(N, D, 1)).view(N, 1), args.tao))

            # calculate inner_batch all similarity
            batch_all = torch.sum(torch.exp(torch.div(torch.mm(query.view(N, D), torch.t(key)), args.tao)), dim=1)

            loss = torch.mean(-torch.log(torch.div(batch_pos, batch_all)))
            return loss
        else: return

    def evaluate_for_batch(self, model, batch):
        vec1 = model.calculate_mask_embedding(input_ids=batch['text_a_token_ids'], attention_mask=batch['text_a_att_mask'])
        vec2 = model.calculate_mask_embedding(input_ids=batch['text_b_token_ids'], attention_mask=batch['text_b_att_mask'])

        sims = F.cosine_similarity(vec1, vec2).cpu().numpy()
        labels = batch['label'].cpu().numpy()

        return compute_corrcoef(sims, labels)

    def evaluate_metrics(self, is_eval, outputs, labels=None, input=None):
        if is_eval:
            return {'spearman correlation': outputs}
        else: return



model = PromptBERT(mask_id)
model.bert.resize_token_embeddings(len(tokenizer))
trainer = PromptBERTTrainer(model, train_dataset, args, eval_data=test_dataset, test_data=test_dataset, validate_every=50, gradient_accumulation_steps=2)

if __name__ == "__main__":
    trainer.train()
    trainer.test()
    # best score is 0.745121

