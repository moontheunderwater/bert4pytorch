#! -*- coding: utf-8 -*-
import os
import sys
import torch
import numpy as np
import torch.nn.functional as F
import torch.utils.checkpoint
sys.path.insert(0, './')
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, DataGenerator, compute_corrcoef
from examples.textmatch.spearman_eval import TestDataset
from bert4pytorch.models.model_building import build_transformer_model

class FinetuningArguments:
    SEED = 42
    learning_rate = 2e-5
    num_train_epochs = 100
    max_len = 80
    batch_size = 64

    metric_key_for_early_stop = "spearman correlation"
    patience = 10

    model_path = "D:/bert4pytorch-master/resources/chinese-roberta-wwm-ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    dataset_name = "STS-B"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/simcse_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()

def load_data(filename):
    D = []
    with open(args.data_dir + filename, "r", encoding="utf-8-sig") as f:
        for line in f.readlines():
            D.append(line.strip('\n'))
    return D

def load_test_data(filename):
    D = []
    with open(args.data_dir + filename, "r", encoding="utf-8-sig") as f:
        for line in f.readlines():
            cache = line.split('||')
            text1, text2, label = cache[1], cache[2], cache[-1]
            D.append((text1, text2, float(label)))
    return D

train_examples = load_data('train-unsup.txt')
test_examples = load_test_data('test.txt')

set_seed(args.SEED)
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
tokenizer = BertTokenizer(args.model_path + "/vocab.txt")

# from transformers import BertTokenizerFast, BertModel, BertConfig
# tokenizer = BertTokenizerFast.from_pretrained(args.model_path)

class MyDataset(DataGenerator):
    def __getitem__(self, index):
        return self.examples[index]


def my_collate_fn(batch):
    new_batch, new_batch_tensor = {}, {}
    new_batch['token_ids'] = []
    new_batch['attention_mask'] = []
    for example in batch:
        inputs = tokenizer.encode_plus(example , max_length=args.max_len)
        input_ids = inputs["input_ids"]
        attention_mask = [1] * len(input_ids)
        input_ids_padded = input_ids + (args.max_len - len(input_ids)) * [0]
        att_mask_padded = attention_mask + (args.max_len - len(attention_mask)) * [0]
        for i in range(2):  # 每个句子重复两次
            new_batch['token_ids'].append(input_ids_padded)
            new_batch['attention_mask'].append(att_mask_padded)

    new_batch_tensor['token_ids'] = torch.tensor(new_batch['token_ids'])  # new_batch为dict, value对应的list需转为tensor
    new_batch_tensor['attention_mask'] = torch.tensor(new_batch['attention_mask'])

    return new_batch_tensor


class ClsModel(torch.nn.Module):
    def __init__(self, ):
        super(ClsModel, self).__init__()
        self.config = BertConfig.from_pretrained(args.model_path)
        self.bert = build_transformer_model(config=self.config, model_path=args.model_path)
        # self.config = BertConfig.from_pretrained(args.model_path+'/config.json')
        # self.bert = BertModel.from_pretrained(args.model_path+'/pytorch_model.bin', config=self.config)

    def forward(self, token_ids=None, attention_mask=None, **inputs):
        encoded_layers, pooled_output = self.bert(input_ids=token_ids, attention_mask=attention_mask, output_hidden_states=True)
        first = encoded_layers[1]
        last = encoded_layers[-1]
        # 第一层和最后一层的隐层取出  然后经过平均池化
        # output = self.bert(input_ids=token_ids, attention_mask=attention_mask, output_hidden_states=True)
        # # return output.pooler_output
        # first = output.hidden_states[1]  # hidden_states列表有13个hidden_state，第一个其实是embeddings，第二个元素才是第一层的hidden_state
        # last = output.hidden_states[-1]

        seq_length = first.size(1)  # 序列长度

        first_avg = torch.avg_pool1d(first.transpose(1, 2), kernel_size=seq_length).squeeze(-1)  # batch, hid_size
        last_avg = torch.avg_pool1d(last.transpose(1, 2), kernel_size=seq_length).squeeze(-1)  # batch, hid_size
        final_encoding = torch.avg_pool1d(
            torch.cat([first_avg.unsqueeze(1), last_avg.unsqueeze(1)], dim=1).transpose(1, 2), kernel_size=2).squeeze(
            -1)
        return final_encoding


class SimcseTrainer(Trainer):
    def compute_loss(self, is_eval, pred, label=None, use_loss=None, input=None):
        if not is_eval:
            idxs = torch.arange(0, pred.shape[0], device=args.device)
            y_true = idxs + 1 - idxs % 2 * 2
            similarities = F.cosine_similarity(pred.unsqueeze(1), pred.unsqueeze(0), dim=2)
            similarities = similarities - torch.eye(pred.shape[0], device=args.device) * 1e12
            similarities = similarities / 0.05
            loss = F.cross_entropy(similarities, y_true)
            return torch.mean(loss)
        else: return

    def evaluate_for_batch(self, model, batch):
        vec1 = model.forward(token_ids=batch['text_a_token_ids'], attention_mask=batch['text_a_att_mask'])
        vec2 = model.forward(token_ids=batch['text_b_token_ids'], attention_mask=batch['text_b_att_mask'])

        sims = F.cosine_similarity(torch.tensor(vec1), torch.tensor(vec2)).cpu().numpy()
        labels = batch['label'].cpu().numpy()

        return compute_corrcoef(sims, labels)

    def evaluate_metrics(self, is_eval, outputs, labels=None, input=None):
        if is_eval:
            return {'spearman correlation': outputs}
        else: return



train_dataset = MyDataset(train_examples)
test_dataset = TestDataset(test_examples, tokenizer, args)

model = ClsModel()

trainer = SimcseTrainer(model, train_dataset, args, eval_data=test_dataset, train_collate_fn=my_collate_fn, validate_every=30)

if __name__ == "__main__":
    trainer.train()
    # best score is 0.700035

