#! -*- coding: utf-8 -*-
import os
import sys
import torch.nn as nn
import torch.utils.checkpoint
import numpy as np
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, l2_normalize, compute_corrcoef
from bert4pytorch.models.model_building import Bert4TorchModel
sys.path.insert(0, './')

class FinetuningArguments:
    SEED = 2022
    learning_rate = 4e-5

    num_train_epochs = 5
    max_len = 32
    batch_size = 512
    metric_key_for_early_stop = "loss"
    patience = 10

    root_model_path = "D:/bert4pytorch-master/resources/chinese_bert_wwm_ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    dataset_name = "BQ_corpus"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/sentence_bert_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
tokenizer = BertTokenizer(args.root_model_path + "/vocab.txt")

total_labels = set()
def load_data(filename):
    D = []
    with open(filename, "r", encoding="utf-8") as f:
        import csv
        reader = csv.reader(f, delimiter=",")
        next(reader)  # Skip header row.
        for line in reader:
            text_a, text_b, label = line[0], line[1], line[-1]
            total_labels.add(label)
            D.append((text_a, text_b, label))
    return D

train_examples = load_data(args.data_dir + 'train.csv')
dev_examples, test_examples = load_data(args.data_dir + 'dev.csv'), load_data(args.data_dir + 'test.csv')
label_map = {l:i for i,l in enumerate(list(total_labels))}
num_labels = len(list(total_labels))


class MyDataset(DataGenerator):
    def __getitem__(self, index):
        examp = self.examples[index]
        text_a, text_b, label = examp[0], examp[1], examp[2]
        label_id = label_map[label]

        text_a_inputs = tokenizer.encode_plus(text_a, add_special_tokens=True, max_length=args.max_len)
        text_a_input_ids = text_a_inputs["input_ids"]
        text_a_att_mask = [1] * len(text_a_input_ids)
        text_b_inputs = tokenizer.encode_plus(text_b, add_special_tokens=True, max_length=args.max_len)
        text_b_input_ids = text_b_inputs["input_ids"]
        text_b_att_mask = [1] * len(text_b_input_ids)

        text_a_input_ids_tensor = sequence_padding(text_a_input_ids, args.max_len)
        text_a_att_mask_tensor = sequence_padding(text_a_att_mask, args.max_len)
        text_b_input_ids_tensor = sequence_padding(text_b_input_ids, args.max_len)
        text_b_att_mask_tensor = sequence_padding(text_b_att_mask, args.max_len)
        # 返回的顺序一定要与model接收的顺序一致
        return {'text_a_token_ids': text_a_input_ids_tensor, 'text_a_att_mask': text_a_att_mask_tensor,
                'text_b_token_ids': text_b_input_ids_tensor, 'text_b_att_mask': text_b_att_mask_tensor,
                'label': label_id}


class ClsModel(Bert4TorchModel):
    def __init__(self, config, model_path, model_name, **kwargs):
        super(ClsModel, self).__init__(config, model_path, model_name, **kwargs)
        self.fc = torch.nn.Linear(config.hidden_size*3, num_labels)

    def forward(self, token_ids=None, segment_ids=None, attention_mask=None, **inputs):
        text_a_token_ids, text_a_att_mask, text_b_token_ids, text_b_att_mask = \
            inputs['text_a_token_ids'], inputs['text_a_att_mask'], inputs['text_b_token_ids'], inputs['text_b_att_mask']
        _, text_a_pooled_out = self.bert(token_ids=text_a_token_ids, attention_mask=text_a_att_mask)
        _, text_b_pooled_out = self.bert(token_ids=text_b_token_ids, attention_mask=text_b_att_mask)
        return text_a_pooled_out, text_b_pooled_out



train_dataset, dev_dataset, test_dataset = MyDataset(train_examples), MyDataset(dev_examples), MyDataset(test_examples)
kwargs = {"with_pool": True, "num_labels": num_labels}
config = BertConfig.from_pretrained(args.root_model_path, **kwargs)
model = ClsModel(config=config, model_path=args.root_model_path, model_name=args.model_name, **kwargs)


class CosentTrainer(Trainer):
    def compute_loss(self, is_eval, pred, label=None, use_loss=None, input=None):
        s1_vec, s2_vec = pred[:2]
        loss_fct = nn.MSELoss()
        output = torch.cosine_similarity(s1_vec, s2_vec)
        loss = loss_fct(output, label)
        return loss

    def evaluate_metrics(self, is_eval, logits, label=None, input=None):
        s1_vec, s2_vec = logits[:2]
        # vec_a, vec_b = l2_normalize(np.array(s1_vec.cpu())), l2_normalize(np.array(s2_vec.cpu()))
        # sims = (vec_a * vec_b).sum(axis=1)
        import torch.nn.functional as F
        sims = F.cosine_similarity(s1_vec, s2_vec).cpu().numpy()
        return {'spearman': compute_corrcoef(sims, label)}


trainer = CosentTrainer(model, train_dataset, args, eval_data=dev_dataset, test_data=test_dataset,
                  label_list=list(label_map.keys()), validate_every=50)
trainer.train()
trainer.test()
