#! -*- coding: utf-8 -*-
import os
import sys
import torch.nn as nn
import torch.utils.checkpoint
sys.path.insert(0, './')
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator
from bert4pytorch.models.model_building import build_transformer_model

class FinetuningArguments:
    SEED = 42
    learning_rate = 4e-5
    num_train_epochs = 1
    max_len = 64
    batch_size = 512

    metric_key_for_early_stop = "accuracy"
    patience = 8
    model_name = "nezha"
    model_path = "D:/bert4pytorch-master/resources/NEZHA"

    dataset_name = "lcqmc"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
tokenizer = BertTokenizer(args.model_path + "/vocab.txt")

total_labels = set()
def load_data(filename):
    D = []
    with open(args.data_dir + filename, "r", encoding="utf-8") as f:
        for line in f.readlines():
            line = line.strip('\n').split('\t')
            text_a, text_b, label = line[0], line[1], line[-1]
            D.append((text_a, text_b, label))
            total_labels.add(label)
            # for BQ_corpus dataset
            # import csv
            # reader = csv.reader(f, delimiter=",")
            # next(reader)  # Skip header row.
            # for line in reader:
            #     text_a, text_b, label = line[0], line[1], line[-1]
            #     total_labels.add(label)
            #     D.append((text_a, label))
            #     D.append((text_b, label))
    return D

train_examples, dev_examples, test_examples= load_data('train.txt'), load_data('dev.txt'), load_data('test.txt')
label_map, num_labels = {l:i for i,l in enumerate(list(total_labels))}, len(list(total_labels))


class MyDataset(DataGenerator):
    def __getitem__(self, index):
        examp = self.examples[index]
        text_a, text_b, label = examp[0], examp[1], examp[2]
        label_id = label_map[label]

        inputs = tokenizer.encode_plus(text_a, text_b, add_special_tokens=True, max_length=args.max_len)
        input_ids, segment_ids = inputs["input_ids"], inputs["token_type_ids"]
        attention_mask = [1] * len(input_ids)

        input_ids_tensor = sequence_padding(input_ids, args.max_len)
        att_mask_tensor = sequence_padding(attention_mask, args.max_len)
        segment_ids_tensor = sequence_padding(segment_ids, args.max_len)

        return {'token_ids': input_ids_tensor, 'attention_mask': att_mask_tensor, 'segment_ids': segment_ids_tensor, 'label': label_id}


class ClsModel(nn.Module):
    def __init__(self):
        super(ClsModel, self).__init__()
        self.config = BertConfig.from_pretrained(args.model_path)
        self.bert = build_transformer_model(config=self.config, model_path=args.model_path, model_name=args.model_name)
        self.fc = nn.Linear(self.config.hidden_size, num_labels)

    def forward(self, token_ids=None, segment_ids=None, attention_mask=None, **inputs):
        _, pooled_output = self.bert(token_ids=token_ids, attention_mask=attention_mask, segment_ids=segment_ids)
        logits = self.fc(pooled_output)
        return logits


train_dataset, dev_dataset, test_dataset = MyDataset(train_examples), MyDataset(dev_examples), MyDataset(test_examples)

model = ClsModel()

trainer = Trainer(model, train_dataset, args, eval_data=dev_dataset, test_data=test_dataset,
                  label_list=list(label_map.keys()), validate_every=10, use_adv='pgd', fp16=True)
# trainer.train()

trainer.test(model)
# prediction_output_file = os.path.join(args.save_path, "test_predictions.csv")
# trainer.predict(prediction_output_file)