#! -*- coding: utf-8 -*-
import os
import sys
import torch
import torch.nn as nn
import json
import numpy as np
import torch.utils.checkpoint
import warnings
warnings.filterwarnings("ignore")
sys.path.insert(0, './')
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, DataGenerator, batch_sequence_padding, token_rematch
from bert4pytorch.models.model_building import build_transformer_model

class FinetuningArguments:
    SEED = 42
    learning_rate = 4e-5
    num_train_epochs = 30
    max_len = 64
    batch_size = 256

    metric_key_for_early_stop = "f1_score"
    patience = 8

    model_name = "bert"  # bert, roberta-base, nezha, chinese-roberta-wwm
    model_path = "D:/bert4pytorch-master/resources/chinese_bert_wwm_ext"

    efficient_global_pointer = False
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    dataset_name = "CMeEE"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
tokenizer = BertTokenizer(args.model_path + "/vocab.txt")

ent2id = {"bod": 0, "dis": 1, "sym": 2, "mic": 3, "pro": 4, "ite": 5, "dep": 6, "dru": 7, "equ": 8}
id2ent = {v:k for k, v in ent2id.items()}
num_labels = len(ent2id.keys())

def load_data(filename):
    D = []
    for d in json.load(open(args.data_dir + filename, encoding='utf-8')):
        D.append([d['text']])
        for e in d['entities']:
            start, end, label = e['start_idx'], e['end_idx'], e['type']
            if start <= end:
                D[-1].append((start, end, ent2id[label]))
    return D

train_examples, dev_examples = load_data('train.json'), load_data('dev.json')

class MyDataset(DataGenerator):
    def __getitem__(self, index):
        return self.examples[index]

def my_collate_fn(examples):
    def encoder(item):
        text = item[0]
        tokens = tokenizer.tokenize(text)
        mapping = token_rematch().rematch(text, tokens)  # 对于英文，一个单词可能对应几个token，不像中文一个token即一个字
        start_mapping = {j[0]: i for i, j in enumerate(mapping) if j}
        end_mapping = {j[-1]: i for i, j in enumerate(mapping) if j}
        encoder_txt = tokenizer.encode_plus(text, max_length=args.max_len)
        input_ids = encoder_txt["input_ids"]
        token_type_ids = encoder_txt["token_type_ids"]
        attention_mask = [1] * len(input_ids)

        return text, start_mapping, end_mapping, input_ids, token_type_ids, attention_mask

    raw_text_list, batch_input_ids, batch_attention_mask, batch_labels, batch_segment_ids = [], [], [], [], []
    for item in examples:
        raw_text, start_mapping, end_mapping, input_ids, token_type_ids, attention_mask = encoder(item)
        labels = np.zeros((num_labels, args.max_len, args.max_len))
        for start, end, label in item[1:]:
            if start in start_mapping and end in end_mapping and start < args.max_len and end < args.max_len:
                start = start_mapping[start]
                end = end_mapping[end]
                labels[label, start, end] = 1  # [start, end]的span为一个entity, 标签为label
        raw_text_list.append(raw_text)
        batch_input_ids.append(input_ids)
        batch_segment_ids.append(token_type_ids)
        batch_attention_mask.append(attention_mask)
        batch_labels.append(labels[:, :len(input_ids), :len(input_ids)])

    input_ids = torch.tensor(batch_sequence_padding(batch_input_ids, length=args.max_len)).long()
    segment_ids = torch.tensor(batch_sequence_padding(batch_segment_ids, length=args.max_len)).long()
    att_mask = torch.tensor(batch_sequence_padding(batch_attention_mask, length=args.max_len)).float()
    labels = torch.tensor(batch_sequence_padding(batch_labels, seq_dims=3)).long()

    return {'input_ids': input_ids, 'attention_mask': att_mask, 'token_type_ids': segment_ids, 'label': labels}

class GlobalPointerModel(nn.Module):
    def __init__(self):
        super(GlobalPointerModel, self).__init__()
        self.config = BertConfig.from_pretrained(args.model_path)
        self.bert = build_transformer_model(config=self.config, model_path=args.model_path)
        self.inner_dim = 64
        self.dense = torch.nn.Linear(768, num_labels * self.inner_dim * 2)
        self.effcient_dense1 = torch.nn.Linear(self.config.hidden_size, self.inner_dim * 2, bias=True)
        self.effcient_dense2 = torch.nn.Linear(self.inner_dim * 2, num_labels * 2, bias=True)

    def sinusoidal_position_embedding(self, batch_size, seq_len, output_dim):
        position_ids = torch.arange(0, seq_len, dtype=torch.float).unsqueeze(-1)
        indices = torch.arange(0, output_dim // 2, dtype=torch.float)
        indices = torch.pow(10000, -2 * indices / output_dim)
        embeddings = position_ids * indices   # [seq_len, output_dim // 2]
        embeddings = torch.stack([torch.sin(embeddings), torch.cos(embeddings)], dim=-1)  # [seq_len, output_dim // 2, 2]
        embeddings = embeddings.repeat((batch_size, *([1]*len(embeddings.shape))))   # [bs, seq_len, output_dim // 2, 2]
        embeddings = torch.reshape(embeddings, (batch_size, seq_len, output_dim))   # [bs, seq_len, output_dim]
        embeddings = embeddings.to(args.device)
        return embeddings

    def forward(self, input_ids=None, token_type_ids=None, attention_mask=None, **inputs):
        encoded_layers, _ = self.bert(
            input_ids=input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids)
        batch_size, seq_len = encoded_layers.size(0), encoded_layers.size(1)

        if args.efficient_global_pointer:
            outputs = self.effcient_dense1(encoded_layers)
            qw, kw = outputs[..., ::2], outputs[..., 1::2]  # (batch_size, seq_len, inner_dim)

            pos_emb = self.sinusoidal_position_embedding(batch_size, seq_len, self.inner_dim)  # pos_emb:(batch_size, seq_len, inner_dim)
            cos_pos = pos_emb[..., None, 1::2].repeat_interleave(2, dim=-1).squeeze(2)  #  (batch_size, seq_len, inner_dim)
            sin_pos = pos_emb[..., None, ::2].repeat_interleave(2, dim=-1).squeeze(2)
            qw2 = torch.stack([-qw[..., 1::2], qw[..., ::2]], -1)  # (batch_size, seq_len, inner_dim/2, 2)
            qw2 = qw2.reshape(qw.shape)  # (batch_size, seq_len, inner_dim)
            qw = qw * cos_pos + qw2 * sin_pos
            kw2 = torch.stack([-kw[..., 1::2], kw[..., ::2]], -1)
            kw2 = kw2.reshape(kw.shape)
            kw = kw * cos_pos + kw2 * sin_pos  # (batch_size, seq_len, inner_dim)

            logits = torch.einsum("bmd,bnd->bmn", qw, kw) / self.inner_dim ** 0.5
            bias = self.effcient_dense2(outputs).transpose(1, 2) / 2  # 'bnh->bhn'
            logits = logits[:, None] + bias[:, ::2, None] + bias[:, 1::2, :, None]  # logits:(batch_size, num_labels, seq_len, seq_len)

        else:
            outputs = self.dense(encoded_layers)  # (batch_size, seq_len, num_labels*inner_dim*2)
            outputs = torch.split(outputs, self.inner_dim * 2, dim=-1)  # tuple: (batch_size, seq_len, inner_dim*2,  ) * num_labels,
            outputs = torch.stack(outputs, dim=-2)  # (batch_size, seq_len, num_labels, inner_dim*2)
            qw, kw = outputs[..., :self.inner_dim], outputs[..., self.inner_dim:]  # qw,kw:(batch_size, seq_len, num_labels, inner_dim)

            pos_emb = self.sinusoidal_position_embedding(batch_size, seq_len, self.inner_dim)  # pos_emb:(batch_size, seq_len, inner_dim)
            # print(pos_emb[..., None, 1::2].size())  # cos_pos,sin_pos: (batch_size, seq_len, 1, inner_dim/2)
            cos_pos = pos_emb[..., None, 1::2].repeat_interleave(2, dim=-1)  # cos_pos,sin_pos: (batch_size, seq_len, 1, inner_dim)
            sin_pos = pos_emb[..., None, ::2].repeat_interleave(2, dim=-1)
            qw2 = torch.stack([-qw[..., 1::2], qw[..., ::2]], -1)  # (batch_size, seq_len, num_labels, inner_dim/2, 2)
            qw2 = qw2.reshape(qw.shape)  # (batch_size, seq_len, num_labels, inner_dim)
            qw = qw * cos_pos + qw2 * sin_pos
            kw2 = torch.stack([-kw[..., 1::2], kw[..., ::2]], -1)
            kw2 = kw2.reshape(kw.shape)
            kw = kw * cos_pos + kw2 * sin_pos  # (batch_size, seq_len, num_labels, inner_dim)
            # 对应原文 https://spaces.ac.cn/archives/8265  公式13

            logit = torch.einsum('bmhd,bnhd->bhmn', qw, kw)  # logits:(batch_size, num_labels, seq_len, seq_len)
            logits = logit / self.inner_dim ** 0.5

            # TODO:add mask!!!

        return logits

train_dataset, dev_dataset = MyDataset(train_examples), MyDataset(dev_examples)

class GlobalPointerTrainer(Trainer):
    def multilabel_categorical_crossentropy(self, y_pred, y_true):
        y_pred = (1 - 2 * y_true) * y_pred  # -1 -> pos classes, 1 -> neg classes
        y_pred_neg = y_pred - y_true * 1e12  # mask the pred outputs of pos classes
        y_pred_pos = y_pred - (1 - y_true) * 1e12  # mask the pred outputs of neg classes
        zeros = torch.zeros_like(y_pred[..., :1])
        y_pred_neg = torch.cat([y_pred_neg, zeros], dim=-1)
        y_pred_pos = torch.cat([y_pred_pos, zeros], dim=-1)
        neg_loss = torch.logsumexp(y_pred_neg, dim=-1)
        pos_loss = torch.logsumexp(y_pred_pos, dim=-1)
        return (neg_loss + pos_loss).mean()

    def compute_loss(self, is_eval, pred, label=None, use_loss=None, input=None):
        batch_size, num_labels = label.shape[:2]
        y_true = label.reshape(batch_size * num_labels, -1)  # (batch_size, num_labels, seq_len, seq_len)
        y_pred = pred.reshape(batch_size * num_labels, -1)  # (batch_size, num_labels, seq_len, seq_len)
        loss = self.multilabel_categorical_crossentropy(y_pred, y_true)
        return loss

    def evaluate_metrics(self, is_eval, outputs, labels=None, input=None):
        y_pred = outputs.data.cpu().numpy()
        y_true = labels.data.cpu().numpy()
        pred = []
        true = []
        for b, l, start, end in zip(*np.where(y_pred > 0)):
            pred.append((b, l, start, end))
        for b, l, start, end in zip(*np.where(y_true > 0)):
            true.append((b, l, start, end))

        R = set(pred)
        T = set(true)
        X = len(R & T)
        Y = len(R)
        Z = len(T)
        f1, precision, recall = 2 * (X + 1e-10) / (Y + Z + 1e-10), (X + 1e-10) / (Y + 1e-10), (X + 1e-10) / (Z + 1e-10)
        return {'precision': precision, 'recall': recall, 'f1_score': f1}


model = GlobalPointerModel()
trainer = GlobalPointerTrainer(model, train_dataset, args, eval_data=dev_dataset,
                  label_list=list(ent2id.keys()), train_collate_fn=my_collate_fn, eval_collate_fn=my_collate_fn, validate_every=200, fp16=False)
trainer.train()
trainer.test()
# f1_score: 0.633584