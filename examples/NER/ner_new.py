 #! -*- coding: utf-8 -*-
import os
import sys
import torch
import json
import numpy as np
import torch.nn as nn
import torch.utils.checkpoint
sys.path.insert(0, './')
from bert4pytorch.layers import CRF
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.trainers.metrics import compute_sequence_labeling_metrics
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, DataReader
from bert4pytorch.models.model_building import build_transformer_model


class FinetuningArguments:
    SEED = 42
    learning_rate = 4e-5
    num_train_epochs = 5
    max_len = 64
    batch_size = 512

    metric_key_for_early_stop = "weighted avg__f1-score"  # ner不要用accuracy
    patience = 8

    model_path = f"D:/bert4pytorch-master/resources/chinese-roberta-wwm-ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    use_crf = True
    if use_crf:
        pad_token_label_id = 0
    else:
        pad_token_label_id = -100

    dataset_name = "ccks_2017"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/ner_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
tokenizer = BertTokenizer(args.model_path + "/vocab.txt")


class ProcessedData(DataReader):
    def load_data(self, filename):
        D, total_labels = [], []
        lines = json.load(open(filename, "r", encoding="utf-8"))

        for (i, line) in enumerate(lines):
            words = line["sentence"]
            # list of spans: [[12, 13, "Symptom"], ....]
            #   结束位置index是内含的
            labels_by_span = line["labeled entities"]
            labels_in_sequence = ["O"] * len(words)

            for span in labels_by_span:
                labels_in_sequence[int(span[0])] = f"B-{span[-1]}"
                labels_in_sequence[int(span[0]) + 1: int(span[1]) + 1] = [f"I-{span[-1]}", ] * (
                            int(span[1]) - int(span[0]))

            assert len(labels_in_sequence) == len(words)

            D.append((words, labels_in_sequence))
            for label in labels_in_sequence:
                total_labels.append(label)

        label_distribution = self.get_label_distribution(total_labels)
        return D, total_labels, label_distribution


processor = ProcessedData(args, traindata='train.json', devdata='dev.json', testdata='test.json')

# 若未传入dev/test data，则不要接收dev/test examples
if args.use_crf:
    train_examples, train_distri, dev_examples, dev_distri, test_examples, test_distri, label_map, num_labels = \
    processor.get_examples(add_label="PAD")
else:
    train_examples, train_distri, dev_examples, dev_distri, test_examples, test_distri, label_map, num_labels = \
    processor.get_examples()
print('train_label_distribution: ', train_distri)
print('test_label_distribution: ', test_distri)


class MyDataset(DataGenerator):
    def __getitem__(self, index):
        examp = self.examples[index]
        words, labels = examp[0], examp[1]

        # 对于NER数据，如果一个词切分为多个subtokens，非起始subtokens采用pad token的ner label
        #   e.g., A-word --> BERT tokenize --> A_1, A_2, A_3: B-PER, PAD, PAD;
        label_ids = []
        subtokens = []
        for word, lab in zip(words, labels):
            word_tokens = tokenizer.tokenize(word)  # 分词  将词输入到bert的tokenizer中去将它转化为bert词表中的tokens  ['i']
            if not word_tokens:
                word_tokens = [tokenizer.unk_token]  # For handling the bad-encoded word
            subtokens.extend(word_tokens)
            label_ids.extend([label_map[lab]] + [args.pad_token_label_id] * (len(word_tokens) - 1))

        # 处理超过长度的样本
        special_tokens_count = 2
        if len(subtokens) > args.max_len - special_tokens_count:
            subtokens = subtokens[:(args.max_len - special_tokens_count)]
            label_ids = label_ids[:(args.max_len - special_tokens_count)]

        subtokens += [tokenizer.sep_token]
        label_ids += [args.pad_token_label_id]  # [SEP] label: pad_token_label_id
        subtokens = [tokenizer.cls_token] + subtokens
        label_ids = [args.pad_token_label_id] + label_ids  # [CLS] label: pad_token_label_id

        # subtokens to ids
        input_ids = tokenizer.convert_tokens_to_ids(subtokens)
        segment_ids = [0] * len(input_ids)
        attention_mask = [1] * len(input_ids)
        input_ids_tensor = sequence_padding(input_ids, args.max_len,)
        att_mask_tensor = sequence_padding(attention_mask, args.max_len)
        segment_ids_tensor = sequence_padding(segment_ids, args.max_len)
        label_ids_tensor = sequence_padding(label_ids, args.max_len, value=args.pad_token_label_id)

        return {'token_ids': input_ids_tensor, 'attention_mask': att_mask_tensor, 'segment_ids': segment_ids_tensor, 'label': label_ids_tensor}


class NerModel(nn.Module):
    def __init__(self):
        super(NerModel, self).__init__()
        self.config = BertConfig.from_pretrained(args.model_path)
        self.bert = build_transformer_model(config=self.config, model_path=args.model_path)
        self.fc = nn.Linear(self.config.hidden_size, num_labels)
        if args.use_crf:
            self.crf_layer = CRF(num_tags=num_labels, batch_first=True)

    def forward(self, token_ids=None, segment_ids=None, attention_mask=None, **inputs):
        encoded_layers, _ = self.bert(token_ids=token_ids, attention_mask=attention_mask, segment_ids=segment_ids)

        # 取最后一个输出层的所有向量经过CRF
        logits = self.fc(encoded_layers)
        if args.use_crf:
            label = inputs['label']
            loss = self.crf_layer(emissions=logits, tags=label, mask=attention_mask.byte(), reduction='mean')
            # reduction='mean' 为对loss进行归一化，否则值很大
            loss = -1 * loss  # negative log-likelihood
            logits_for_pred = self.crf_layer.decode(logits)
            return (loss, logits, logits_for_pred)
        else:
            logits_for_pred = np.argmax(logits.detach().cpu().numpy(), axis=-1)
            return (logits, logits_for_pred)


train_dataset, dev_dataset, test_dataset = MyDataset(train_examples), MyDataset(dev_examples), MyDataset(test_examples)
model = NerModel()


class SequenceLabelingTrainer(Trainer):
    def compute_loss(self, pred, label=None, use_loss=None, input=None):
        if args.use_crf:
            loss = pred[0]
            return loss
        else:
            logits = pred[0]
            loss_fct = torch.nn.CrossEntropyLoss()
            loss = loss_fct(logits.view(-1, num_labels), label.view(-1))
        return loss

    def evaluate_metrics(self, preds, labels=None, input=None):
        labels = labels.data.cpu().numpy()
        preds = preds[-1]  # logits_for_pred
        id2label = {v:k for k,v in label_map.items()}
        out_label_list = [[] for _ in range(labels.shape[0])]
        preds_list = [[] for _ in range(labels.shape[0])]
        for i in range(labels.shape[0]):     #   pad token label id 对应的预测结果是不需要的
            for j in range(labels.shape[1]):
                if labels[i, j] != args.pad_token_label_id:
                    out_label_list[i].append(id2label[labels[i][j]])
                    preds_list[i].append(id2label[preds[i][j]])

        result = compute_sequence_labeling_metrics(preds_list, out_label_list)
        return result


trainer = SequenceLabelingTrainer(model, train_dataset, args, eval_data=dev_dataset, test_data=test_dataset,
                  label_list=list(label_map.keys()), validate_every=20, fp16=True, use_adv='fgm')
# trainer.train()
trainer.test(model)

