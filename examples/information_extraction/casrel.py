#! -*- coding: utf-8 -*-
import os
import sys
import json
import torch
import torch.nn.functional as F
import torch.nn as nn
import random
from collections import defaultdict
import torch.utils.checkpoint
from bert4pytorch.configs.configuration_bert import BertConfig
from examples.information_extraction.casrel_metrics import CasRelMetrics
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from torch.nn.utils.rnn import pad_sequence
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, DataReader
from bert4pytorch.models.model_building import build_transformer_model
sys.path.insert(0, './')

class FinetuningArguments:
    SEED = 42
    learning_rate = 1.5e-5
    num_train_epochs = 10
    max_len = 100
    batch_size = 128

    metric_key_for_early_stop = "loss"
    patience = 8

    model_name = "bert"
    model_path = "D:/bert4pytorch-master/resources/chinese_bert_wwm_ext"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    dataset_name = "baidu_info_extraction"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()
set_seed(args.SEED)
tokenizer = BertTokenizer(args.model_path + "/vocab.txt")
# from transformers import BertTokenizer, BertModel
# tokenizer = BertTokenizer.from_pretrained('bert-base-chinese')

def load_data(filename):
    D = []
    with open(args.data_dir + filename, "r", encoding="utf-8") as f:
        for line in f.readlines():
            content = json.loads(line.strip('\n'))
            text = content['text']
            spo_list = content['spo_list']
            D.append((text, spo_list))
    return D

train_examples, dev_examples, test_examples= load_data('train.json'), load_data('dev.json'), load_data('dev.json')
total_rel = json.load(open(args.data_dir+'rel.json',encoding='utf-8'))
id2rel = {int(k):v for k,v in total_rel.items()}
rel2id = {v:k for k,v in id2rel.items()}
num_labels = len(id2rel.values())   # 加载自己的total_label,因为有的label可能从未在数据中出现过


class MyDataset(DataGenerator):
    def find_head_idx(self, source, target):
        target_len = len(target)
        for i in range(len(source)):
            if source[i: i + target_len] == target:
                return i
        return -1

    def __getitem__(self, index):
        examp = self.examples[index]
        text, spo_list = examp[0], examp[1]
        inputs = tokenizer.encode_plus(text, add_special_tokens=True, max_length=args.max_len)
        input_ids = inputs["input_ids"]
        att_mask = [1.] * len(input_ids)
        token_ids = torch.tensor(input_ids, dtype=torch.long)
        att_mask = torch.tensor(att_mask, dtype=torch.long)

        text_len = len(input_ids)
        sub_heads, sub_tails = torch.zeros(text_len), torch.zeros(text_len)  # 记录head/tail位置的向量，head/tail位置值为1
        sub_head, sub_tail = torch.zeros(text_len), torch.zeros(text_len)
        obj_heads = torch.zeros((text_len, num_labels))  # obj head/tail_idx - relation_label 处为1
        obj_tails = torch.zeros((text_len, num_labels))

        s2ro_map = defaultdict(list)
        for spo in spo_list:
            triple = (tokenizer.encode_plus(spo['subject'], add_special_tokens=False)['input_ids'],
                      rel2id[spo['predicate']],
                      tokenizer.encode_plus(spo['object'], add_special_tokens=False)['input_ids'])
            sub_head_idx = self.find_head_idx(input_ids, triple[0])  # subject head 在句子中的位置
            obj_head_idx = self.find_head_idx(input_ids, triple[2])
            if sub_head_idx != -1 and obj_head_idx != -1:
                sub = (sub_head_idx, sub_head_idx + len(triple[0]) - 1)
                s2ro_map[sub].append(
                    (obj_head_idx, obj_head_idx + len(triple[2]) - 1, triple[1]))

        if s2ro_map:  # {sub: (obj, relation)}
            for s in s2ro_map:
                sub_heads[s[0]] = 1
                sub_tails[s[1]] = 1
            # print(sub_tails)
            # 随机选择一个subject的起始与终止为止, 这样做的理由是增加了训练集中的负样本数量
            sub_head_idx, sub_tail_idx = random.choice(list(s2ro_map.keys()))

            sub_head[sub_head_idx] = 1
            sub_tail[sub_tail_idx] = 1

            for ro in s2ro_map.get((sub_head_idx, sub_tail_idx), []):  # ro: (obj_head_idx, obj_tail_idx, relation_label_id)
                obj_heads[ro[0]][ro[2]] = 1
                obj_tails[ro[1]][ro[2]] = 1

        return token_ids, att_mask, sub_heads, sub_tails, sub_head, sub_tail, obj_heads, obj_tails, spo_list


def my_collate_fn(batch):
    batch = list(filter(lambda x: x is not None, batch))
    token_ids, masks, sub_heads, sub_tails, sub_head, sub_tail, obj_heads, obj_tails, spo_list = zip(*batch)
    batch_token_ids = pad_sequence(token_ids, batch_first=True)
    batch_masks = pad_sequence(masks, batch_first=True)
    batch_sub_heads = pad_sequence(sub_heads, batch_first=True)
    batch_sub_tails = pad_sequence(sub_tails, batch_first=True)
    batch_sub_head = pad_sequence(sub_head, batch_first=True)
    batch_sub_tail = pad_sequence(sub_tail, batch_first=True)
    batch_obj_heads = pad_sequence(obj_heads, batch_first=True)
    batch_obj_tails = pad_sequence(obj_tails, batch_first=True)

    return {"input_ids": batch_token_ids, "attention_mask": batch_masks, "sub_head": batch_sub_head, "sub_tail": batch_sub_tail,
            "sub_heads": batch_sub_heads, "sub_tails": batch_sub_tails, "obj_heads": batch_obj_heads, "obj_tails": batch_obj_tails, 'spo_list':spo_list}


class CasRelModel(nn.Module):
    '''
    第一步要识别出句子中的 subject, 第二步要根据识别出的 subject, 识别出所有有可能的 relation 以及对应的 object
    '''
    def __init__(self):
        super(CasRelModel, self).__init__()
        self.config = BertConfig.from_pretrained(args.model_path)
        self.bert = build_transformer_model(config=self.config, model_path=args.model_path)
        self.sub_heads_linear = nn.Linear(self.config.hidden_size, 1)
        self.sub_tails_linear = nn.Linear(self.config.hidden_size, 1)
        self.obj_heads_linear = nn.Linear(self.config.hidden_size, num_labels)
        self.obj_tails_linear = nn.Linear(self.config.hidden_size, num_labels)


    def get_subs(self, encoded_text):
        pred_sub_heads = torch.sigmoid(self.sub_heads_linear(encoded_text))  # sigmoid为二分类,判断句子某个idx是否为sub/obj的idx
        pred_sub_tails = torch.sigmoid(self.sub_tails_linear(encoded_text))  # [batch, seq_len, 1] 1维度为sigmmoid后的值，大于0.5属于类别1
        return pred_sub_heads, pred_sub_tails

    def get_objs_for_specific_sub(self, sub_head_mapping, sub_tail_mapping, encoded_text):
        # sub_head_mapping [batch, 1, seq] * encoded_text [batch, seq, dim] = [batch, 1, dim]
        sub_head = torch.matmul(sub_head_mapping, encoded_text)
        sub_tail = torch.matmul(sub_tail_mapping, encoded_text)
        sub = (sub_head + sub_tail) / 2
        encoded_text = encoded_text + sub  # subject与句子向量相加，判断object
        pred_obj_heads = torch.sigmoid(self.obj_heads_linear(encoded_text))
        pred_obj_tails = torch.sigmoid(self.obj_tails_linear(encoded_text))
        return pred_obj_heads, pred_obj_tails

    def forward(self, input_ids=None, token_type_ids=None, attention_mask=None, **inputs):
        sub_head, sub_tail = inputs['sub_head'], inputs['sub_tail']
        # encoded_text = self.bert(token_ids=token_ids, attention_mask=attention_mask)[0]
        encoded_text = self.bert(input_ids=input_ids, attention_mask=attention_mask)[0]
        pred_sub_heads, pred_sub_tails = self.get_subs(encoded_text)

        sub_head_mapping = sub_head.unsqueeze(1)
        sub_tail_mapping = sub_tail.unsqueeze(1)
        pred_obj_heads, pre_obj_tails = self.get_objs_for_specific_sub(sub_head_mapping, sub_tail_mapping, encoded_text)
        return {"pred_sub_heads": pred_sub_heads, "pred_sub_tails": pred_sub_tails, "pred_obj_heads": pred_obj_heads,
                "pred_obj_tails": pre_obj_tails}


train_dataset, dev_dataset, test_dataset = MyDataset(train_examples),  MyDataset(dev_examples), MyDataset(test_examples)


class CasRelTrainer(Trainer):
    def compute_loss(self, is_eval, pred, label=None, use_loss=None, input=None):
        mask = input['attention_mask']
        def loss_fct(logist, label, mask):
            count = torch.sum(mask)
            logist = logist.view(-1)
            label = label.view(-1)
            mask = mask.view(-1)
            focal_weight = torch.where(torch.eq(label, 1), 1 - logist, logist)
            loss = -(torch.log(logist) * label + torch.log(1 - logist) * (1 - label)) * mask
            return torch.sum(focal_weight * loss) / count
        # pred为CasRelModel的返回, input为Mydataset中的batch, 前者为我们随机选一个sub,模型预测的sub/obj 向量，后者为对应的label(sub/obj idx有则为1)
        rel_mask = mask.unsqueeze(-1).repeat(1, 1, num_labels)
        return loss_fct(pred['pred_sub_heads'], input['sub_heads'], mask) + \
               loss_fct(pred['pred_sub_tails'], input['sub_tails'], mask) + \
               loss_fct(pred['pred_obj_heads'], input['obj_heads'], rel_mask) + \
               loss_fct(pred['pred_obj_tails'], input['obj_tails'], rel_mask)

    def evaluate_metrics(self, is_eval, outputs, labels=None, input=None):
        return CasRelMetrics(outputs, input)


model = CasRelModel()
trainer = CasRelTrainer(model, train_dataset, args, train_collate_fn=my_collate_fn, eval_collate_fn=my_collate_fn,fp16=False,
                        eval_data=dev_dataset, test_data=test_dataset, validate_every=100)
trainer.train()
trainer.test()

