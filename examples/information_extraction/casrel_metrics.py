import torch
import pandas as pd
import json

from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer

# total_rel = json.load(open(f"D:/bert4pytorch-master/datasets/baidu_info_extraction/"+'rel.json',encoding='utf-8'))
# id2rel = {int(k):v for k,v in total_rel.items()}
# rel2id = {v:k for k,v in id2rel.items()}
# root_model_path = "D:/bert4pytorch-master/resources/NEZHA"
# tokenizer = BertTokenizer(root_model_path + "/vocab.txt")

def convert_score_to_zero_one(tensor):
    # tensor[tensor >= 0.5] = 1
    # tensor[tensor < 0.5] = 0
    return torch.where(tensor>=0.5,1,0)

def extract_sub(pred_sub_heads, pred_sub_tails):
    subs = []
    heads = torch.arange(0, len(pred_sub_heads))[pred_sub_heads == 1]
    tails = torch.arange(0, len(pred_sub_tails))[pred_sub_tails == 1]

    for head, tail in zip(heads, tails):
        if tail >= head:
            subs.append((head.item(), tail.item()))
    return subs


def extract_obj_and_rel(obj_heads, obj_tails):
    obj_heads = obj_heads.T
    obj_tails = obj_tails.T
    rel_count = obj_heads.shape[0]
    obj_and_rels = []

    for rel_index in range(rel_count):
        obj_head = obj_heads[rel_index]
        obj_tail = obj_tails[rel_index]

        objs = extract_sub(obj_head, obj_tail)
        if objs:
            for obj in objs:
                start_index, end_index = obj
                obj_and_rels.append((rel_index, start_index, end_index))
    return obj_and_rels


def CasRelMetrics(pred, inputs):
    df = pd.DataFrame(columns=['TP', 'PRED', "REAL", 'p', 'r', 'f1'], index=['sub', 'triple'])
    df.fillna(0, inplace=True)
    batch_size = inputs['input_ids'].shape[0]

    pred_sub_heads = convert_score_to_zero_one(pred['pred_sub_heads'])  #.squeeze()
    pred_sub_tails = convert_score_to_zero_one(pred['pred_sub_tails'])  #.squeeze()
    sub_heads = inputs['sub_heads']
    sub_tails = inputs['sub_tails']

    pred_obj_heads = convert_score_to_zero_one(pred['pred_obj_heads'])
    pred_obj_tails = convert_score_to_zero_one(pred['pred_obj_tails'])  # bs,seq_len,num_labels
    obj_heads = inputs['obj_heads']
    obj_tails = inputs['obj_tails']

    total_pred, total_real, total_tp = 0, 0, 0

    for batch_index in range(batch_size):
        pred_subs = extract_sub(pred_sub_heads[batch_index].squeeze(), pred_sub_tails[batch_index].squeeze())
        true_subs = extract_sub(sub_heads[batch_index].squeeze(), sub_tails[batch_index].squeeze())

        pred_objs = extract_obj_and_rel(pred_obj_heads[batch_index], pred_obj_tails[batch_index])
        true_objs = extract_obj_and_rel(obj_heads[batch_index], obj_tails[batch_index])

        df['PRED']['sub'] += len(pred_subs)
        df['REAL']['sub'] += len(true_subs)
        for true_sub in true_subs:
            if true_sub in pred_subs:
                df['TP']['sub'] += 1

        df['PRED']['triple'] += len(pred_objs)
        df['REAL']['triple'] += len(true_objs)
        total_pred += len(pred_objs)
        total_real += len(true_objs)
        # if pred_objs != []:
        #     for item in pred_objs:
        #         rel = id2rel[int(item[0])]
        #         tokens = inputs['token_ids'][batch_index].tolist()
        #         sub = tokenizer.decode(tokens[int(item[1]):int(item[2])+1])
        #         print(rel, sub)

        for true_obj in true_objs:
            if true_obj in pred_objs:
                total_tp += 1
                df['TP']['triple'] += 1

    df.loc['sub', 'p'] = df['TP']['sub'] / (df['PRED']['sub'] + 1e-9)
    df.loc['sub', 'r'] = df['TP']['sub'] / (df['REAL']['sub'] + 1e-9)
    df.loc['sub', 'f1'] = 2 * df['p']['sub'] * df['r']['sub'] / (df['p']['sub'] + df['r']['sub'] + 1e-9)

    sub_precision = df['TP']['sub'] / (df['PRED']['sub'] + 1e-9)
    sub_recall = df['TP']['sub'] / (df['REAL']['sub'] + 1e-9)
    sub_f1 = 2 * sub_precision * sub_recall / (sub_precision + sub_recall + 1e-9)

    df.loc['triple', 'p'] = df['TP']['triple'] / (df['PRED']['triple'] + 1e-9)
    df.loc['triple', 'r'] = df['TP']['triple'] / (df['REAL']['triple'] + 1e-9)
    df.loc['triple', 'f1'] = 2 * df['p']['triple'] * df['r']['triple'] / (
            df['p']['triple'] + df['r']['triple'] + 1e-9)

    triple_precision = df['TP']['triple'] / (df['PRED']['triple'] + 1e-9)
    triple_recall = df['TP']['triple'] / (df['REAL']['triple'] + 1e-9)
    triple_f1 = 2 * triple_precision * triple_recall / (
            triple_precision + triple_recall + 1e-9)

    return {'precision': triple_precision,
            'recall': triple_recall,
            'f1_score': triple_f1}
