#! -*- coding: utf-8 -*-
import os
import sys
import torch.nn as nn
import torch.utils.checkpoint
sys.path.insert(0, './')
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.trainers.train_func import Trainer
from bert4pytorch.snippets import set_seed, sequence_padding, DataGenerator, load_bert_vocab
from bert4pytorch.models.model_building import Bert4TorchModel
from bert4pytorch.models.modeling_bert import BertLMPredictionHead


class FinetuningArguments:
    SEED = 42
    learning_rate = 3.5e-5
    num_train_epochs = 30
    max_len = 40
    batch_size = 1

    metric_key_for_early_stop = "loss"
    patience = 8

    model_name = "nezha"
    root_model_path = "D:/bert4pytorch-master/resources/NEZHA"
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    dataset_name = "kuakua_qa"
    data_dir = f"D:/bert4pytorch-master/datasets/{dataset_name}/"

    save_path = f"D:/bert4pytorch-master/experiments/outputs/{model_name}_{dataset_name}_{SEED}"
    os.makedirs(save_path, exist_ok=True)

args = FinetuningArguments()


def load_data(filename):
    D = []
    with open(filename, "r", encoding="utf-8") as f:
        lines = []
        text_pair = []
        for line in f.readlines():
            line = line.strip('\n').split('\t')
            if 'Q:' in line[0]:
                text_pair.append(line[1])
            if 'A:' in line[0]:
                text_pair.append(line[1])
                lines.append(text_pair)
                text_pair = []

    for (i, line) in enumerate(lines):
        if line == []:
            pass
        text_a, text_b = line[0], line[1]
        D.append((text_a, text_b))
    return D


train_examples = load_data(args.data_dir + 'train.txt')
dev_examples = load_data(args.data_dir + 'train.txt')

set_seed(args.SEED)
# tokenizer = BertTokenizer(args.root_model_path + "/vocab.txt")
from transformers import BertTokenizerFast, BertModel
# tokenizer = BertTokenizer.from_pretrained(args.root_model_path)
tokenizer = BertTokenizerFast.from_pretrained(args.root_model_path)

word2ix = load_bert_vocab(args.root_model_path + "/vocab.txt")

class MyDataset(DataGenerator):
    def __getitem__(self, index):
        examp = self.examples[index]
        text_a, text_b = examp[0], examp[1]
        inputs = tokenizer.encode_plus(text_a, text_b, add_special_tokens=True, max_length=args.max_len)
        input_ids, segment_ids = inputs["input_ids"], inputs["token_type_ids"]

        return input_ids, segment_ids


def my_collate_fn(batch):
    """
    根据batch句子maxlen动态padding， batch为一部分sample
    """
    def padding(indice, max_length, pad_idx=0):
        """
        pad 函数
        注意 token type id 右侧pad是添加1而不是0，1表示属于句子B
        """
        pad_indice = [item + [pad_idx] * max(0, max_length - len(item)) for item in indice]
        return torch.LongTensor(pad_indice).to(args.device)

    token_ids = [data[0] for data in batch]
    segment_ids = [data[1] for data in batch]
    token_ids_padded = padding(token_ids, args.max_len)
    segment_ids_padded = padding(segment_ids, args.max_len)
    target_ids_padded = token_ids_padded[:, 1:].contiguous()


    ones = torch.ones((1, 1, args.max_len, args.max_len), dtype=torch.float32, device=args.device)
    a_mask = ones.tril()  # 下三角矩阵
    s_ex12 = segment_ids_padded.unsqueeze(1).unsqueeze(2).float()
    s_ex13 = segment_ids_padded.unsqueeze(1).unsqueeze(3).float()  # 一个是列向量，一个是行向量，bs维度不参与计算

    att_mask = ((1.0 - s_ex12) * (1.0 - s_ex13) + s_ex13 * a_mask)  # [bs,1,len,len]

    return {'token_ids': token_ids_padded, 'attention_mask': att_mask, 'segment_ids': segment_ids_padded, 'label': target_ids_padded}


class UnilmModel(nn.Module):
    def __init__(self):
        super(UnilmModel, self).__init__()
        self.transform = BertModel.from_pretrained(args.root_model_path)
        self.decoder = BertLMPredictionHead(config, self.bert.embeddings.word_embeddings.weight)

    def compute_loss(self, predictions, labels, target_mask):
        """
        target_mask : 句子a部分和pad部分全为0， 而句子b部分为1
        """
        predictions = predictions.view(-1, config.vocab_size)
        labels = labels.view(-1)
        target_mask = target_mask.view(-1).float()
        loss_fct = nn.CrossEntropyLoss(ignore_index=0, reduction="none")
        return (loss_fct(predictions, labels) * target_mask).sum() / target_mask.sum()  ## 通过mask 取消 pad 和句子a部分预测的影响

    def forward(self, token_ids=None, segment_ids=None, attention_mask=None, **kwargs):
        encoded_layers = self.transform(input_ids=token_ids, attention_mask=attention_mask, token_type_ids=segment_ids)[0]
        # encoded_layers, _ = self.bert(token_ids=token_ids, attention_mask=attention_mask, segment_ids=segment_ids)
        squence_out = self.dropout(encoded_layers)

        predictions = self.decoder(squence_out)   # torch.Size([bs, seq_len, vocab_size])
        return predictions


class UnilmTrainer(Trainer):
    def compute_loss(self, pred, label=None, use_loss=None, input=None):
        segment_ids, label_ids = input['segment_ids'], input['label']

        predictions = pred[:, :-1].contiguous()   # 预测的值不用取最后sep符号的结果 因此是到-1
        target_mask = segment_ids[:, 1:].contiguous()  # 需要构建特殊的输出mask 才能计算正确的loss

        predictions = predictions.view(-1, config.vocab_size)
        labels = label_ids.view(-1)
        target_mask = target_mask.view(-1).float()    #  target_mask : 句子a部分和pad部分全为0， 而句子b部分为1
        loss_fct = nn.CrossEntropyLoss(ignore_index=0, reduction="none")
        return (loss_fct(predictions, labels) * target_mask).sum() / target_mask.sum()  ## 通过mask 取消 pad 和句子a部分预测的影响

    def evaluate_metrics(self, pred, labels=None, input=None):
        return


train_dataset, dev_dataset = MyDataset(train_examples), MyDataset(dev_examples)

kwargs = {"with_pool": True}
config = BertConfig.from_pretrained(args.root_model_path, **kwargs)
model = UnilmModel(config=config, model_path=args.root_model_path, model_name=args.model_name, **kwargs)

trainer = UnilmTrainer(model, train_dataset, args, eval_data=dev_dataset, validate_every=10, fp16=False, train_collate_fn=my_collate_fn)
trainer.train()

class Decoder():
    def __init__(self, tokenizer, word2ix, model):
        super().__init__()
        self.tokenizer = tokenizer
        self.word2ix = word2ix
        self.model = model
        output_model_file = os.path.join(args.save_path, "model.bin")
        self.model.load_state_dict(torch.load(output_model_file, map_location=args.device))


    def generate(self, text, out_max_length=30, beam_size=1, device="cuda"):
        # 对一个句子生成相应的结果
        # 通过输出最大长度得到输入的最大长度，这里问题不大，如果超过最大长度会进行截断
        self.out_max_length = out_max_length
        input_max_length = args.max_len - out_max_length
        inputs = tokenizer.encode_plus(text, max_length=input_max_length)
        token_ids, token_type_ids = inputs["input_ids"], inputs["token_type_ids"]

        token_ids = torch.tensor(token_ids, device=device).view(1, -1)
        token_type_ids = torch.tensor(token_type_ids, device=device).view(1, -1)
        out_puts_ids = self.beam_search(token_ids, token_type_ids, self.word2ix, beam_size=beam_size)
        # 解码 得到相应输出
        return self.tokenizer.decode(out_puts_ids)

    def beam_search(self, token_ids, token_type_ids, word2ix, beam_size=1, device="cuda"):
        sep_id = word2ix["[SEP]"]
        # 用来保存输出序列
        output_ids = [[]]
        # 用来保存累计得分
        output_scores = torch.zeros(token_ids.shape[0], device=device)
        for step in range(self.out_max_length):
            scores = self.model.forward(token_ids, token_type_ids, device=device)   #  [bs, len, vocab_size]
            if step == 0:
                # 重复beam-size次 输入ids
                token_ids = token_ids.view(1, -1).repeat(beam_size, 1)
                token_type_ids = token_type_ids.view(1, -1).repeat(beam_size, 1)
            ## 计算log 分值 (beam_size, vocab_size)
            logit_score = torch.log_softmax(scores, dim=-1)[:, -1]
            logit_score = output_scores.view(-1, 1) + logit_score  # 累计得分
            ## 取topk的时候我们是展平了然后再去调用topk函数
            # 展平
            logit_score = logit_score.view(-1)
            hype_score, hype_pos = torch.topk(logit_score, beam_size)
            indice1 = hype_pos / scores.shape[-1]  # 行索引
            indice2 = hype_pos % scores.shape[-1]  # 列索引

            # 下面需要更新一下输出了
            new_hype_scores = []
            new_hype_ids = []
            # 为啥有这个[],就是因为要过滤掉结束的序列。
            next_chars = []  # 用来保存新预测出来的一个字符，继续接到输入序列后面，再去预测新字符
            for i_1, i_2, score in zip(indice1, indice2, hype_score):
                i_1 = int(i_1.item())
                i_2 = int(i_2.item())

                hype_id = output_ids[i_1] + [i_2]  # 保存所有输出的序列，而不仅仅是新预测的单个字符

                if i_2 == sep_id:
                    # 说明解码到最后了
                    if score == torch.max(hype_score).item():
                        # 说明找到得分最大的那个序列了 直接返回即可
                        return hype_id[: -1]
                    else:
                        # 完成一个解码了，但这个解码得分并不是最高，因此的话需要跳过这个序列
                        beam_size -= 1
                else:
                    new_hype_ids.append(hype_id)
                    new_hype_scores.append(score)
                    next_chars.append(i_2)  # 收集一下，需要连接到当前的输入序列之后

            output_ids = new_hype_ids

            output_scores = torch.tensor(new_hype_scores, dtype=torch.float32, device=device)
            # 现在需要重新构造输入数据了，用上一次输入连接上这次新输出的字符，再输入bert中预测新字符
            token_ids = token_ids[:len(output_ids)].contiguous()  # 截取，因为要过滤掉已经完成预测的序列
            token_type_ids = token_type_ids[: len(output_ids)].contiguous()

            next_chars = torch.tensor(next_chars, dtype=torch.long, device=device).view(-1, 1)
            next_token_type_ids = torch.ones_like(next_chars, device=device)
            # 连接
            token_ids = torch.cat((token_ids, next_chars), dim=1)
            token_type_ids = torch.cat((token_type_ids, next_token_type_ids), dim=1)
            if beam_size < 1:
                break

        # 如果达到最大长度的话 直接把得分最高的输出序列返回把
        return output_ids[output_scores.argmax().item()]

decoder = Decoder(tokenizer, word2ix, model)
print(decoder.generate('要去打球赛了求表扬'))
