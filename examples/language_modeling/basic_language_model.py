#! -*- coding: utf-8 -*-
import os

from torch.nn import CrossEntropyLoss
from torch.utils.data import Dataset, DataLoader

import torch
import torch.nn as nn
import torch.utils.checkpoint
import json
import time

import sys
sys.path.insert(0, './')
from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.tokenizers.tokenization_bert import BertTokenizer
from bert4pytorch.models.model_building import build_transformer_model

class BaiscLMArguments:
    SEED = 100
    max_len = 128
    model_name = "nezha"
    root_model_path = "D:/bert4pytorch-master/resources/NEZHA"


args = BaiscLMArguments()

# 建立分词器
tokenizer = BertTokenizer(args.root_model_path + "/vocab.txt", never_split=["[MASK]", "[CLS]", "[SEP]", "[UNK]"])
sentence = "北京[MASK]安门广场"

inputs = tokenizer.encode_plus(
    sentence,
    add_special_tokens=True,
    max_length=args.max_len,
)
tokens_ids, segments_ids = inputs["input_ids"], inputs["token_type_ids"]
attention_mask = [1] * len(tokens_ids)
mask_position = tokens_ids.index(tokenizer.mask_token_id)

tokens_ids_tensor = torch.tensor([tokens_ids])
attention_mask_tensor = torch.tensor([attention_mask])
segment_ids_tensor = torch.tensor([segments_ids])


# 加载config
#   需要传入参数 with_mlm
kwargs = {
    "with_mlm": True,
    "max_sequence": 128,
}
config = BertConfig.from_pretrained(
    args.root_model_path,
    **kwargs
)
model = build_transformer_model(
    config=config,
    model_path=args.root_model_path,
    model_name=args.model_name,
    **kwargs
)

# 预测掩码位置的词本应该是什么
model.eval()
output = model(
    token_ids=tokens_ids_tensor,
    attention_mask=attention_mask_tensor,
    segment_ids=segment_ids_tensor,
)
result = torch.argmax(output[0, mask_position]).item()
print(tokenizer.convert_ids_to_tokens([result])[0])
