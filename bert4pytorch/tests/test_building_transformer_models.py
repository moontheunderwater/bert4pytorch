#! -*- coding: utf-8 -*-
# Author: Michael
# Date: 2022-01-23
# Version: v0.1
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""PyTorch test modeling module. """

import sys
sys.path.append("./")

from bert4pytorch.configs.configuration_bert import BertConfig
from bert4pytorch.models.model_building import build_transformer_model


if __name__ == "__main__":

    # # 测试是否能够加载 NEZHA 模型
    # model_path = "./resources/NEZHA/"
    # # 加载config
    # kwargs = {
    #     "with_pool": True,
    #     "max_sequence": 128,
    # }
    # config = BertConfig.from_pretrained(
    #     model_path,
    #     **kwargs
    # )
    #
    # print("config: ", config)
    # build_transformer_model(
    #     config=config,
    #     model_path=model_path,
    #     model_name='nezha',
    #     **kwargs
    # )

    # 测试能否正常加载 BERT 模型
    model_path = "./resources/chinese_bert_wwm_ext"
    # 加载config
    kwargs = {
        "with_pool": True,
        "max_sequence": 128,
    }
    config = BertConfig.from_pretrained(
        model_path,
        **kwargs
    )

    print("config: ", config)
    build_transformer_model(
        config=config,
        model_path=model_path,
        model_name='bert',
        **kwargs
    )