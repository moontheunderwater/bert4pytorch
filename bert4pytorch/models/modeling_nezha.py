#! -*- coding: utf-8 -*-
# Author: Michael
# Date: 2022-01-23
# Version: v0.1
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""PyTorch Nezha model. """


import torch
import torch.nn as nn
import copy
from bert4pytorch.layers import LayerNorm, PositionWiseFeedForward, activations, \
    NezhaAttention, NezhaEmbeddings
from bert4pytorch.models.modeling_bert import BertModel


class NezhaLayer(nn.Module):
    """
        Transformer层:
        顺序为: Attention --> Add --> LayerNorm --> Feed Forward --> Add --> LayerNorm

        注意: 1、以上都不计dropout层，并不代表没有dropout，每一层的dropout使用略有不同，注意区分
              2、原始的Transformer的encoder中的Feed Forward层一共有两层linear，
              config.intermediate_size的大小不仅是第一层linear的输出尺寸，也是第二层linear的输入尺寸
    """
    def __init__(self, config, ):
        super(NezhaLayer, self).__init__()
        self.multiHeadAttention = NezhaAttention(config)
        self.dropout1 = nn.Dropout(config.hidden_dropout_prob)
        self.layerNorm1 = LayerNorm(config.hidden_size, eps=1e-12)
        self.feedForward = PositionWiseFeedForward(config)
        self.dropout2 = nn.Dropout(config.hidden_dropout_prob)
        self.layerNorm2 = LayerNorm(config.hidden_size, eps=1e-12)

    def forward(self, hidden_states, attention_mask):
        self_attn_output, layer_attn = self.multiHeadAttention(hidden_states, attention_mask)
        hidden_states = hidden_states + self.dropout1(self_attn_output)
        hidden_states = self.layerNorm1(hidden_states)
        self_attn_output2 = self.feedForward(hidden_states)
        hidden_states = hidden_states + self.dropout2(self_attn_output2)
        hidden_states = self.layerNorm2(hidden_states)
        return hidden_states


class NezhaModel(BertModel):
    """
    构建 Nezha 模型
    """

    def __init__(self,
            config,
    ):
        super(NezhaModel, self).__init__(config, )

        self.use_relative_position = config.use_relative_position

        self.embeddings = NezhaEmbeddings(
            config,
        )
        layer = NezhaLayer(
            config,
        )
        self.encoderLayer = nn.ModuleList([copy.deepcopy(layer) for _ in range(self.num_hidden_layers)])
        if self.with_pool:
            # Pooler部分（提取CLS向量）
            self.pooler = nn.Linear(self.hidden_size, self.hidden_size)
            self.pooler_activation = nn.Tanh()
            if self.with_nsp:
                # Next Sentence Prediction部分
                # nsp的输入为pooled_output, 所以with_pool为True是使用nsp的前提条件
                self.nsp = nn.Linear(self.hidden_size, 2)
        else:
            self.pooler = None
            self.pooler_activation = None
        if self.with_mlm:
            self.mlmDecoder = nn.Linear(self.hidden_size, self.vocab_size, bias=False)
            # self.mlmDecoder.weight = self.embeddings.word_embeddings.weight
            self.mlmBias = nn.Parameter(torch.zeros(self.vocab_size))
            self.mlmDecoder.bias = self.mlmBias
            self.mlmDense = nn.Linear(self.hidden_size, self.hidden_size)
            self.transform_act_fn = activations[self.hidden_act]
            self.mlmLayerNorm = LayerNorm(self.hidden_size, eps=1e-12)
        self.apply(self.init_model_weights)


    def forward(self, token_ids=None,
                segment_ids=None,
                attention_mask=None,
                output_all_encoded_layers=False):
        """
            token_ids： token id 序列
            segment_ids： 就是token对应的句子id,值为0或1（0表示对应的token属于第一句，1表示属于第二句）,当
                             任务只有一个句子输入时，segment_ids的每个值都是0，可不用传值
            attention_mask：各元素的值为0或1,避免在padding的token上计算attention, 1进行attetion, 0不进行attention

            以上三个参数的shape为： (batch_size, sequence_length); type为tensor
        """

        if attention_mask is None:
            # 根据token_ids创建一个3D的attention mask矩阵，尺寸为[batch_size, 1, 1, to_seq_length]，
            # 目的是为了适配多头注意力机制，从而能广播到[batch_size, num_heads, from_seq_length, to_seq_length]尺寸
            attention_mask = (token_ids != 0).long().unsqueeze(1).unsqueeze(2)
        if segment_ids is None:
            segment_ids = torch.zeros_like(token_ids)

        # 兼容fp16
        attention_mask = attention_mask.to(dtype=next(self.parameters()).dtype)
        # 对mask矩阵中，数值为0的转换成很大的负数，使得不需要attention的位置经过softmax后,分数趋近于0
        # attention_mask = (1.0 - attention_mask) * -10000.0
        # 执行embedding
        hidden_states = self.embeddings(token_ids, segment_ids)
        # 执行encoder
        encoded_layers = [hidden_states] # 添加embedding的输出
        for layer_module in self.encoderLayer:
            # hidden_states = layer_module(hidden_states, attention_mask)

            def create_custom_forward(module):
                def custom_forward(*inputs):
                    return module(*inputs, )

                return custom_forward

            hidden_states = torch.utils.checkpoint.checkpoint(
                create_custom_forward(layer_module),
                hidden_states,
                attention_mask,
            )

            if output_all_encoded_layers:
                encoded_layers.append(hidden_states)
        if not output_all_encoded_layers:
            encoded_layers.append(hidden_states)

        # 获取最后一层隐藏层的输出
        sequence_output = encoded_layers[-1]
        # 是否取最后一层输出
        if not output_all_encoded_layers:
            encoded_layers = encoded_layers[-1]

        # 是否添加pool层
        if self.with_pool:
            pooled_output = self.pooler_activation(self.pooler(sequence_output[:, 0]))
        else:
            pooled_output = None
        # 是否添加nsp
        if self.with_pool and self.with_nsp:
            nsp_scores = self.nsp(pooled_output)
        else:
            nsp_scores = None
        # 是否添加mlm
        if self.with_mlm:
            mlm_hidden_state = self.mlmDense(sequence_output)
            mlm_hidden_state = self.transform_act_fn(mlm_hidden_state)
            mlm_hidden_state = self.mlmLayerNorm(mlm_hidden_state)
            mlm_scores = self.mlmDecoder(mlm_hidden_state)
        else:
            mlm_scores = None
        # 根据情况返回值
        if mlm_scores is None and nsp_scores is None:
            return encoded_layers, pooled_output
        elif mlm_scores is not None and nsp_scores is not None:
            return mlm_scores, nsp_scores
        elif mlm_scores is not None:
            return mlm_scores
        else:
            return nsp_scores

    def variable_mapping(self):
        mapping = {
            'embeddings.word_embeddings.weight': 'bert.embeddings.word_embeddings.weight',
            # 'embeddings.position_embeddings.weight': 'bert.embeddings.position_embeddings.weight',
            'embeddings.token_type_embeddings.weight': 'bert.embeddings.token_type_embeddings.weight',
            # 'embeddings.layerNorm.weight': 'bert.embeddings.LayerNorm.gamma',
            'embeddings.layerNorm.weight': 'bert.embeddings.LayerNorm.weight',
            'embeddings.layerNorm.bias': 'bert.embeddings.LayerNorm.bias',
            # 'embeddings.layerNorm.bias': 'bert.embeddings.LayerNorm.beta',
            'pooler.weight': 'bert.pooler.dense.weight',
            'pooler.bias': 'bert.pooler.dense.bias',
            'nsp.weight': 'cls.seq_relationship.weight',
            'nsp.bias': 'cls.seq_relationship.bias',
            'mlmDense.weight': 'cls.predictions.transform.dense.weight',
            'mlmDense.bias': 'cls.predictions.transform.dense.bias',
            # 'mlmLayerNorm.weight': 'cls.predictions.transform.LayerNorm.gamma',
            'mlmLayerNorm.weight': 'cls.predictions.transform.LayerNorm.weight',
            # 'mlmLayerNorm.bias': 'cls.predictions.transform.LayerNorm.beta',
            'mlmLayerNorm.bias': 'cls.predictions.transform.LayerNorm.bias',
            'mlmBias': 'cls.predictions.bias',
            'mlmDecoder.weight': 'cls.predictions.decoder.weight',
            'mlmDecoder.bias': 'cls.predictions.decoder.bias'

        }
        for i in range(self.num_hidden_layers):
            prefix = 'bert.encoder.layer.%d.' % i
            mapping.update({'encoderLayer.%d.multiHeadAttention.self.query.weight' % i: prefix + 'attention.self.query.weight',
                            'encoderLayer.%d.multiHeadAttention.self.query.bias' % i: prefix + 'attention.self.query.bias',
                            'encoderLayer.%d.multiHeadAttention.self.key.weight' % i: prefix + 'attention.self.key.weight',
                            'encoderLayer.%d.multiHeadAttention.self.key.bias' % i: prefix + 'attention.self.key.bias',
                            'encoderLayer.%d.multiHeadAttention.self.value.weight' % i: prefix + 'attention.self.value.weight',
                            'encoderLayer.%d.multiHeadAttention.self.value.bias' % i: prefix + 'attention.self.value.bias',
                            'encoderLayer.%d.multiHeadAttention.self.output.weight' % i: prefix + 'attention.output.dense.weight',
                            'encoderLayer.%d.multiHeadAttention.self.output.bias' % i: prefix + 'attention.output.dense.bias',
                            # 'encoderLayer.%d.layerNorm1.weight' % i: prefix + 'attention.output.LayerNorm.gamma',
                            'encoderLayer.%d.layerNorm1.weight' % i: prefix + 'attention.output.LayerNorm.weight',
                            # 'encoderLayer.%d.layerNorm1.bias' % i: prefix + 'attention.output.LayerNorm.beta',
                            'encoderLayer.%d.layerNorm1.bias' % i: prefix + 'attention.output.LayerNorm.bias',
                            'encoderLayer.%d.feedForward.intermediateDense.weight' % i: prefix + 'intermediate.dense.weight',
                            'encoderLayer.%d.feedForward.intermediateDense.bias' % i: prefix + 'intermediate.dense.bias',
                            'encoderLayer.%d.feedForward.outputDense.weight' % i: prefix + 'output.dense.weight',
                            'encoderLayer.%d.feedForward.outputDense.bias' % i: prefix + 'output.dense.bias',
                            # 'encoderLayer.%d.layerNorm2.weight' % i: prefix + 'output.LayerNorm.gamma',
                            'encoderLayer.%d.layerNorm2.weight' % i: prefix + 'output.LayerNorm.weight',
                            # 'encoderLayer.%d.layerNorm2.bias' % i: prefix + 'output.LayerNorm.beta'
                            'encoderLayer.%d.layerNorm2.bias' % i: prefix + 'output.LayerNorm.bias'
                            })

        return mapping