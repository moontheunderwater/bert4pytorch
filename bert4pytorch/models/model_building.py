#! -*- coding: utf-8 -*-


import os
import torch.nn as nn
from bert4pytorch.models.modeling_bert import BertModel
from bert4pytorch.models.modeling_nezha import NezhaModel
from bert4pytorch.models.modeling_roberta import RobertaModel

def build_transformer_model(
        config=None,
        model_path=None,
        model_name=None,
):
    """
    根据配置文件构建模型，可选加载checkpoint权重
    """
    if model_name == None:
        try:
            model_name = config.model_type
        except AttributeError:
            raise AttributeError('Please pass the model_name parameter!')

    models = {
        'bert': BertModel,
        'roberta': RobertaModel,
        'nezha': NezhaModel,
    }

    my_model = models[model_name]
    # print("kwargs: ", kwargs)
    transformer = my_model(config)

    # for n, p in transformer.named_parameters():
    #     print(n, p.requires_grad)

    checkpoint_path = os.path.join(model_path, "pytorch_model.bin")
    transformer.load_weights_from_pytorch_checkpoint(checkpoint_path)

    return transformer


class Bert4TorchModel(nn.Module):
    def __init__(self, config, model_path, model_name, **kwargs):
        super(Bert4TorchModel, self).__init__()
        self.bert = build_transformer_model(
            config=config,
            model_path=model_path,
            model_name=model_name
        )
        self.config = config
        self.dropout = nn.Dropout(p=0.3)

        try:
            self.fc = nn.Linear(config.hidden_size, config.num_labels)
        except AttributeError:
            print('BertConfig object has no attribute num_labels')


    def forward(self, **kwargs):
        raise NotImplementedError()